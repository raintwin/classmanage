//
//  FetchEbookViewController.m
//  DMPK
//
//  Created by jiaodian on 12-11-9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "FetchEbookViewController.h"
#import "InstanceView.h"
#import "MyScrollView.h"
#import "InstanceObject.h"
#import "SVProgressHUD.h"
#import "BDMultiDownloader.h"
#import "UIImageView+WebCache.h"
#define TOTAL_ROW_NUMBER 5
#define PICK_ROW_NUMBER 1
#define DOWNTAG  10000

@interface FetchEbookViewController ()
@property (nonatomic,retain)UIView *pickView;
@property (nonatomic,retain)NSMutableArray *plansArray;
@property (nonatomic,retain) UITableView *imageTableView;
@property (nonatomic,retain)PlanObject *mPlanObject;
@property (nonatomic,retain)UIScrollView *scrollview;

@end

@implementation FetchEbookViewController
@synthesize planId;
@synthesize mEBooks;
@synthesize isBook;
@synthesize pickView;

@synthesize plansArray;
@synthesize imageTableView;
@synthesize mPlanObject;
@synthesize scrollview;
@synthesize mDownLoadImageDict;


-(void)dealloc
{
    [mEBooks release];
    [pickView release];
    [plansArray release];
    [mPlanObject release];
    [scrollview release];
    
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mEBooks = nil;
    self.pickView = nil;
    self.plansArray = nil;
    self.mPlanObject = nil;
    self.scrollview = nil;

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self.view setBackgroundColor:[UIColor blackColor]];
        pageIndex = -1;
        kNumberOfPages = -1;
        mainScreen = CGRectZero;
        
    }
    return self;
}
-(void)CancelDownLoad
{
    for (int i = 0; i<[mEBooks count]; i++) {
        //        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:i inSection:0];
        DownLoadScrollImage *imageDownLoad = [mDownLoadImageDict objectForKey:[NSNumber numberWithInt:i]];
        if (imageDownLoad != nil) {
            [imageDownLoad CancelDownloadImage];
        }
        
    }
    [mDownLoadImageDict removeAllObjects];
}

-(void)CloseViewController
{
    [self CancelDownLoad];
    [SVProgressHUD dismiss];
    
    [self.view removeFromSuperview];
    [self release];
}
#pragma mark  load imageview 

#pragma mark

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.0) {
        //5.0及以后，不整这个，界面错位 整这个带动画的话，容易看到一个白头
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
    NSLog(@"mEBooks count = %d",[mEBooks count]);
    self.plansArray = [[NSMutableArray alloc]init];
    NSMutableDictionary *dict = (NSMutableDictionary*) [[NSUserDefaults standardUserDefaults] objectForKey:NOTIFICATION_QUESTPLAN];
    
    self.isBook = [[dict objectForKey:@"isBook"] boolValue];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIView animateWithDuration:0.0f
                     animations:^{
                         [self.view setTransform: CGAffineTransformMakeRotation(M_PI / 2)];
                         
                         self.view.frame = CGRectMake(0, 0, 768, 1024);

                     }];
    
    self.scrollview = [[UIScrollView alloc]initWithFrame:self.view.frame];
    self.scrollview.delegate = self;
    [self.scrollview setPagingEnabled:YES];
    [self.scrollview setContentSize:CGSizeMake([mEBooks count]*768, 0)];
    [self.view addSubview:self.scrollview];
    
    {
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        self.mDownLoadImageDict = mDict;
        [mDict release];
    }
    pageIndex = 0;
    kNumberOfPages = [mEBooks count];
    NSLog(@"ebooks cout %d",[mEBooks count]);
    [self loadScrollViewWithPage:pageIndex];

//    [self loadImageView:0];
//    self.imageTableView = [[UITableView alloc]initWithFrame:self.view.frame];
//    self.imageTableView.dataSource = self;
//    self.imageTableView.delegate = self;
//    [self.imageTableView setPagingEnabled:YES];
//    [self.imageTableView setTransform: CGAffineTransformMakeRotation(-M_PI / 2)];
//    [self.imageTableView setFrame:CGRectMake(0, 0, 1024, 1024)];
//    [self.view addSubview:self.imageTableView];
    
    if (self.isBook) {
        UIButton *button = [InstanceView addUIButtonByImage:self.view image:@"planButton.png" rect:CGRectMake(184, 955, 379, 64)];
        [button addTarget:self action:@selector(openPlan) forControlEvents:64];
    }
    
    UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"关闭.png" point:CGPointMake(0, 0)];
    
    [mButton addTarget:self action:@selector(CloseViewController) forControlEvents:64];
    
    
}

#pragma mark tableview

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.mEBooks.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 1024;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewScrollPositionNone;
    }
    
    
    FetchEbook *pObj = [mEBooks objectAtIndex:[indexPath row]];

    
    cell.imageView.image = nil;
    [cell.imageView setTransform: CGAffineTransformMakeRotation(-M_PI / 2)];

    //Here's a pattern for updating table cells with data:
    
    if (pObj.image!=nil) {
        //if the datasource item is already loaded, just update the cell and return.
        cell.imageView.image = pObj.image;
        
    }else{
        //if the data needs to be downloaded, do it.
        //here's using the convenient method for loading images
        if (pObj.isLoad) {
            return cell;
        }
        pObj.isLoad = YES;
        NSLog(@"[album valueForKey:kvImageLink] = %@",pObj.url);
        [[BDMultiDownloader shared] imageWithPath:pObj.url
                                       completion:^(UIImage *image, BOOL fromCache) {
                                           //save downloaded image to the datasource item.
                                           pObj.image = image;
                                           //animate the newly loaded image on the table view.
                                           //and animate only if the image is not from cache (first loaded.)
                                           UITableViewRowAnimation animation = fromCache?UITableViewScrollPositionNone:UITableViewRowAnimationAutomatic;
                                           if ([self _isIndexPathVisible:indexPath]) {
                                               [self.imageTableView canCancelContentTouches];
                                               [self.imageTableView beginUpdates];
                                               [self.imageTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                                                     withRowAnimation:animation];
                                               [self.imageTableView endUpdates];
                                           }
                                       }];
    }
    
    
    return cell;
}

- (BOOL)_isIndexPathVisible:(NSIndexPath *)indexPath
{
    NSArray *visibleIndexPaths = [self.imageTableView indexPathsForVisibleRows];
    return [visibleIndexPaths containsObject:indexPath];
}

#pragma mark
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
//    [self loadImageView:page];
    
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
}
#pragma mark  load imageview
-(void)StartDownLoadImage:(NSString*)url  tag:(int)index
{
    NSLog(@"Start index = %d",index);

    DownLoadScrollImage *imageDownLoad = [mDownLoadImageDict objectForKey:[NSNumber numberWithInt:index]];
    if (imageDownLoad == nil) {
        [SVProgressHUD dismiss];
        [SVProgressHUD show];
        imageDownLoad = [[DownLoadScrollImage alloc] init];
        imageDownLoad.tag = index;
        imageDownLoad.ImageURL = url;
        imageDownLoad.ImageHeight = mainScreen.size.height;
        imageDownLoad.ImageWidth = mainScreen.size.width;
        imageDownLoad.Delegate =self;
        [mDownLoadImageDict setObject:imageDownLoad forKey:[NSNumber numberWithInt:index]];
        [imageDownLoad StartDownloadImage];
        [imageDownLoad release];
    }
}
-(void)FinishDownLoadImageOfScrooll:(int)index Image:(UIImage*)image IsFailed:(BOOL)isFaild
{
//    UIScrollView *mScrollView = (UIScrollView *)[self.view viewWithTag:110011];
    [SVProgressHUD dismiss];

    DownLoadScrollImage *imageDownLoad = [mDownLoadImageDict objectForKey:[NSNumber numberWithInt:index]];
    if (imageDownLoad != nil) {
        NSLog(@"index = %d",index);
        FetchEbook *obj = [mEBooks objectAtIndex:index];
        
        if (isFaild) {
            obj.image = [UIImage imageNamed:@"暂无信息横.png"];
            obj.isLoad = NO;
        }
        else
        {
            obj.image = image;
            obj.isLoad = YES;
            
        }
        MyScrollView *imageview = (MyScrollView*)[self.scrollview viewWithTag:DOWNTAG+index];
        if (imageview!=nil) {
            if (obj.image!=nil) {
                [imageview setImage:obj.image];
            }
            
        }
        
    }
}
- (void)loadScrollViewWithPage:(int)page
{
    if (page < 0)
    { return;}
    if (page >= kNumberOfPages)
    { return ;}

    
    CGRect frame = self.scrollview.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    
    MyScrollView *ascrView = (MyScrollView*)[self.scrollview viewWithTag:DOWNTAG+page];
    FetchEbook *pObj = [mEBooks objectAtIndex:page];
    
    if (ascrView == nil)
    {
        NSLog(@"pObj.url = %@",pObj.url);
        ascrView = [[MyScrollView alloc] initWithFrame:frame];
        ascrView.tag = DOWNTAG+page;
        [self.scrollview addSubview:ascrView];
        [ascrView.imageView setImageWithURL:[NSURL URLWithString:pObj.url]
                          placeholderImage:[UIImage imageNamed:@"common_default_320x240.png"]];
        [ascrView release];

    }
    
}
-(void)setScrollImageView:(UIImage *)image ascrView:(MyScrollView *)ascrView
{
    [ascrView setImage:image];
}
#pragma mark 打开教案
-(void)openPlan
{
    
    NSMutableDictionary *dict = (NSMutableDictionary*) [[NSUserDefaults standardUserDefaults] objectForKey:NOTIFICATION_QUESTPLAN];

    
    HttpFormatRequest *httpRequest = [[HttpFormatRequest alloc]init];
    httpRequest.Delegate = self;
    
    [self.view addSubview:httpRequest];

    

    [httpRequest CustomFormDataRequestDict:[dict JSONRepresentation] tag:0 url:[InstanceView FormatUrl:@"ClientPlan"]];
    
    [httpRequest release];
    

}
#pragma mark pick delegate
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.plansArray.count;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    self.mPlanObject = self.plansArray[row];

    return self.mPlanObject.name;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    self.mPlanObject = self.plansArray[row];

//    NSLog(@"self.textArray[row] = %@",self.plansArray[row]);
//    
//    self.mPlanObject  = [self.plansArray objectAtIndex:row];
//    
//    NSMutableDictionary *dictDefaults = (NSMutableDictionary*) [[NSUserDefaults standardUserDefaults] objectForKey:NOTIFICATION_QUESTPLAN];
//
//    
//    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
//    [dict setObject:[NSNumber numberWithInt:mPlanObject.planId] forKey:@"planId"];
//    [dict setObject:[dictDefaults objectForKey:@"catalogId"] forKey:@"catalogId"];
//    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isBook"];
//    
//    NSLog(@"dict = %@",dict);
//    [[NSUserDefaults standardUserDefaults] setObject:dict forKey:NOTIFICATION_QUESTPLAN];
//    
//    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_PLAN object:self];
//    [self CloseViewController];
}

#pragma mark--


-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"网络错误，请重新尝试" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    NSArray *Plans = [[[ResultContent JSONValue] objectForKey:@"plan"] retain];
    if ([Plans count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"没有教案" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        [Plans release];
        return;
    }
    for (int i = 0; i<[Plans count]; i++) {
        PlanObject *planOject = [[PlanObject alloc]init];
        planOject.planId = [[NSString stringWithFormat:@"%@",[[Plans objectAtIndex:i] valueForKey:@"planId"]] intValue];
        planOject.name = [NSString stringWithFormat:@"%@",[[Plans objectAtIndex:i] valueForKey:@"name"]];
        [self.plansArray addObject:planOject];
        [planOject release];
    }
    [Plans release];
    
    if (self.pickView == nil) {
        self.pickView = [[UIView alloc]initWithFrame:CGRectMake(0, 741, 768, 283)];
        [self.pickView setBackgroundColor:[UIColor blackColor]];
        [self.view addSubview:self.pickView];
        
        UIPickerView *myPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 768, 300)];
        myPickerView.showsSelectionIndicator = YES;
        myPickerView.delegate = self;
        myPickerView.dataSource = self;
        [self.pickView addSubview:myPickerView];
        [myPickerView release];
        
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0,216, 768, 67)];
        imageView.image = [UIImage imageNamed:@"pick.png"];
        [self.pickView addSubview:imageView];
        [imageView release];
        
        {
            UIButton *button = [InstanceView addUIButtonByImage:self.pickView image:@"cancelPlanButton.png" rect:CGRectMake(119, 220, 249, 51)];
            [button addTarget:self action:@selector(cancelPlan) forControlEvents:64];
        }
        {
            UIButton *button = [InstanceView addUIButtonByImage:self.pickView image:@"confirmPlanButton.png" rect:CGRectMake(400, 220, 249, 51)];
            [button addTarget:self action:@selector(confirmPlan) forControlEvents:64];
        }
    }
}

#pragma mark 关闭教案
-(void)cancelPlan
{
    if (self.pickView != nil) {
        [self.pickView removeFromSuperview];
    }
}
-(void)confirmPlan
{
    
    
    NSMutableDictionary *dict = (NSMutableDictionary*) [[NSUserDefaults standardUserDefaults] objectForKey:NOTIFICATION_QUESTPLAN];

    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    
    [dictionary setObject:[NSNumber numberWithInt:self.mPlanObject.planId] forKey:@"planId"];
    [dictionary setObject:[dict objectForKey:@"catalogId"] forKey:@"catalogId"];
    [dictionary setObject:[NSNumber numberWithBool:NO] forKey:@"isBook"];


    
    //    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    //    [dict setObject:[NSNumber numberWithInt:mPlanObject.planId] forKey:@"planId"];
    //    [dict setObject:[dictDefaults objectForKey:@"catalogId"] forKey:@"catalogId"];
    //    [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isBook"];
    
    NSLog(@"dict = %@",dict);
    [[NSUserDefaults standardUserDefaults] setObject:dictionary forKey:NOTIFICATION_QUESTPLAN];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_PLAN object:self];
    [dictionary release];
    [self CloseViewController];
}




@end
