//
//  SyllabusViewController.h
//  DMPK
//
//  Created by jiaodian on 12-10-30.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SyllabusScrollView.h"
#import "SearchClassroomViewController.h"
@interface SyllabusViewController : UIViewController<HttpFormatRequestDeleagte,UITableViewDataSource,UITableViewDelegate,SyllabusScrollViewDelegate>

{
    int isSingleWeek;
    int mDays;
}
@property(nonatomic,assign)int mTeacherId;
@property(nonatomic,retain)NSMutableArray *mDataArray;
@property(nonatomic,retain)NSMutableDictionary *mDataDict;

@property(nonatomic,retain)HttpFormatRequest *mHttpRequest;


@end
