//
//  ViewController.m
//  DMPK
//
//  Created by jiaodian on 12-10-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "LoginViewCtrl.h"
#import "SetViewController.h"
#import "InstanceView.h"
@interface ViewController ()

@end

@implementation ViewController
@synthesize mNavigationController;
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    LoginViewCtrl *loginview = [[LoginViewCtrl alloc]init];    

    UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:loginview];
    [nav setNavigationBarHidden:YES];
    [nav.view setFrame:self.view.bounds];	
    self.mNavigationController = nav;
    [loginview release];
    [nav release];
    
    
    [self.view addSubview:mNavigationController.view];
    
//    [self.navigationController pushViewController:loginview animated:YES];
    
    
    [InstanceView addUIImageViewOfPoint:self.view imageFile:@"蓝色底.png" point:CGPointMake(0,709)];
    {
        UIButton *mSetButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"设置.png" point:CGPointMake(30, 714)];
        [mSetButton addTarget:self action:@selector(Seting) forControlEvents:64];
    }
    [mTeatherInfoDict setObject:[NSNumber numberWithInt:1] forKey:WEBINFOSTRING];
}
-(void)Seting
{
    SetViewController *mSearchClassroom = [[SetViewController alloc]initWithNibName:@"SetViewController" bundle:nil];
    [mSearchClassroom.view setBackgroundColor:[UIColor clearColor]];
    [mSearchClassroom.view setFrame:[[UIScreen mainScreen] bounds]];
    [self.view addSubview:mSearchClassroom.view];
//    [mSearchClassroom release];

}

-(void)dealloc
{
    [mNavigationController release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.mNavigationController = nil;
}

//-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
//{
//    if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
//        [[UIDevice currentDevice] performSelector:@selector(setOrientation:)
//                                       withObject:(id)UIDeviceOrientationLandscapeLeft];
//    }
//}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (interfaceOrientation== UIDeviceOrientationPortrait) NSLog(@"Up");
    
    if(interfaceOrientation == UIDeviceOrientationPortraitUpsideDown) NSLog(@"Down");

    if ( interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown ) {
        return NO;
    }
    else {
        return YES;
    } 

}
-(BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}
@end
