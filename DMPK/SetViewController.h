//
//  SetViewController.h
//  DMPK
//
//  Created by jiaodian on 12-11-9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetViewController : UIViewController<UIAlertViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIAlertViewDelegate,HttpFormatRequestDeleagte>
{
    int mSetType;
}

@property(nonatomic,retain)HttpFormatRequest *mHttpRequest;

@end
