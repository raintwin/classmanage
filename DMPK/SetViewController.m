//
//  SetViewController.m
//  DMPK
//
//  Created by jiaodian on 12-11-9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "SetViewController.h"
#import "InstanceView.h"
#import "ViewController.h"
#define MessageTag  2200011
#define VersionTag  2200022

#define TableviewHeight 38
@interface SetViewController ()

@end

@implementation SetViewController
@synthesize mHttpRequest;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        mSetType = 0;
    }
    return self;
}
-(BOOL)writerDict
{

    UITableView *mTableView =(UITableView *)[self.view viewWithTag:10101011];    
    NSIndexPath *mIndexPath1 = [NSIndexPath indexPathForRow:0 inSection:0];
    NSIndexPath *mIndexPath2 = [NSIndexPath indexPathForRow:1 inSection:0];

    UITableViewCell *mCellUrl = [mTableView cellForRowAtIndexPath:mIndexPath1];
    UITableViewCell *mCellPost = [mTableView cellForRowAtIndexPath:mIndexPath2];

    UITextField *mTextField1 = ( UITextField *)[mCellUrl viewWithTag:101];
    UITextField *mTextField2 = ( UITextField *)[mCellPost viewWithTag:102];

        NSLog(@"URL:%@",mTextField1.text);
        if ([mTextField1.text length] == 0) {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"地址不能为空" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            alertView.tag = 11001;
            alertView.delegate =self;
            [alertView show];
            [alertView release];
            return NO;
        }
    
        NSLog(@"POST:%@",mTextField2.text);
        if ([mTextField2.text length] == 0) {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"端口不能为空" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            alertView.tag = 11001;
            alertView.delegate =self;
            [alertView show];
            [alertView release];
            return NO;
        }
    NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
    [mDict setObject:mTextField1.text forKey:WEBURLSTRKEY];
    [mDict setObject:mTextField2.text forKey:WEBPOSTSTRKEY];
    [mDict writeToFile:[InstanceView GetDocumentByString:WEBINFOSTRING] atomically:YES];
    [mDict release];
    return YES;
}
#pragma mark 关闭
-(void)CloseViewControll
{
    if (mSetType == 1) {
        if (![self writerDict]) {
            return;
        };
    }
    [self.view removeFromSuperview];
    [self release];
}
-(void)SendMessage:(NSString*)MessageText
{
    NSString *str = @"";
    if ([MessageText length]>0) {
        str = MessageText;
    }
    else {
        str = @" ";
    }
    {
        mHttpRequest.Delegate = self;
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:str forKey:@"content"];
        [mDict setObject:[NSNumber numberWithInt:1] forKey:@"flag"];
        NSString *string = [mDict JSONRepresentation];
        [mDict release];
        [mHttpRequest CustomFormDataRequestDict:string tag:MessageTag url:[InstanceView FormatUrl:@"ClientMessage"]];
    }

}
-(void)SendPassWord:(NSString*)MessageText
{
    NSString *str = @"";
    if ([MessageText length]>0) {
        str = MessageText;
    }
    else {
        str = @" ";
    }
    {
        mHttpRequest.Delegate = self;
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:MessageText forKey:@"password"];
        [mDict setObject:[mTeatherInfoDict objectForKey:@"teacherId"] forKey:@"teacherId"];
        NSString *string = [mDict JSONRepresentation];
        [mDict release];
        [mHttpRequest CustomFormDataRequestDict:string tag:MessageTag url:[InstanceView FormatUrl:@"ClientChangePassword"]];
    }

}

-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    if (tag == MessageTag) {
        return;
    }
    if (tag == VersionTag) {
        NSString *string = [NSString stringWithFormat:@" %@ \n\n 佛山市爱皮皮科技有限公司",[[ResultContent JSONValue] valueForKey:@"versionNo"]];
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"版本信息\n课堂教堂管理系统" message:string delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
        [alertView release];
    }
}
-(NSString*)GetWebInfoForKey:(NSString*)string
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:WEBINFOSTRING];
    if ([[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:documentDirectory];
        return [dic objectForKey:string];
    }
    return nil;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [InstanceView addUIImageViewOfPoint:self.view imageFile:@"设置背景.png" point:CGPointMake(0, 0)];

    
    {
        HttpFormatRequest *http = [[HttpFormatRequest alloc]init];
        http.Delegate = self;
        self.mHttpRequest = http;
        [http release];
        [self.view addSubview:mHttpRequest];

    }
    UITableView *mTableView = [[UITableView alloc]initWithFrame:CGRectMake(200, 150, 510, 350) style:UITableViewStyleGrouped];
    [mTableView setCenter:CGPointMake(512, 350)];
    [mTableView setScrollEnabled:NO];
    [mTableView setBackgroundColor:HEXCOLOR(0xf7f7f7)];
    mTableView.tag = 10101011;
    mTableView.delegate = self;
    mTableView.dataSource = self;
    [self.view addSubview:mTableView];
    [mTableView release];

    UIButton *button = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"叉.png" point:CGPointMake(740, 105)];
    [button addTarget:self action:@selector(CloseViewControll) forControlEvents:64];
    
    
    
    mSetType = [[mTeatherInfoDict objectForKey:WEBINFOSTRING] intValue];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *cellName = @"cellstr";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    
    if (cell== nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
        cell.textLabel.text = @"";
    }
    [cell setBackgroundColor:[UIColor whiteColor]];
    cell.textLabel.text = @"";
    if (mSetType == 1) {
        if ([indexPath section] == 0) {
            NSArray *arry =[[NSArray alloc]initWithObjects:@"地址:",@"端口:", nil];
            cell.textLabel.text =[arry objectAtIndex:[indexPath row]];
            [arry release];
            if ([indexPath row] == 0) {
                UITextField *mTextUrl = [InstanceView addTextFileld:CGRectMake(60, 3, 300, TableviewHeight-5)];
                [mTextUrl setBorderStyle:UITextBorderStyleRoundedRect];
                mTextUrl.text = [self GetWebInfoForKey:WEBURLSTRKEY];
                mTextUrl.tag  = 101;
                [cell.contentView addSubview:mTextUrl];
                [mTextUrl release];
            }
            if ([indexPath row] == 1) {
                UITextField *mTextUrl = [InstanceView addTextFileld:CGRectMake(60, 3, 300, TableviewHeight-5)];
                [mTextUrl setBorderStyle:UITextBorderStyleRoundedRect];
                mTextUrl.text = [self GetWebInfoForKey:WEBPOSTSTRKEY];
                mTextUrl.tag  = 102;
                [cell.contentView addSubview:mTextUrl];
                [mTextUrl release];
            }
        }
        if ([indexPath section] == 1) {
            cell.textLabel.text = @"还原默认值";

        }
        return cell;
    }
    if (mSetType == 2) {
        if ([indexPath section] == 0) {
            NSArray *arry =[[NSArray alloc]initWithObjects:@"注销登录",@"修改密码", nil];
            
            cell.textLabel.text =[arry objectAtIndex:[indexPath row]];
            [arry release];
        }
        if ([indexPath section] == 1) {
            NSArray *arry =[[NSArray alloc]initWithObjects:@"意见反馈",@"版权信息", nil];
            cell.textLabel.text = [arry objectAtIndex:[indexPath row]];
            [arry release];
        }
        return cell;
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if (mSetType ==1) {
       return  section == 1? 1:2;
        
    }
    if (mSetType ==2) {
        return 2;
    }
    return 0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;              // Default is 1 if not implemented
{
    if (mSetType == 1) {
        return 2;
    }
    if (mSetType == 2) {
        return 2;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TableviewHeight;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (mSetType == 1) {
        if ([indexPath section] ==1) {
            UITableView *mTableView =(UITableView *)[self.view viewWithTag:10101011];    
            NSIndexPath *mIndexPath1 = [NSIndexPath indexPathForRow:0 inSection:0];
            NSIndexPath *mIndexPath2 = [NSIndexPath indexPathForRow:01 inSection:0];
            
            UITableViewCell *mCellUrl = [mTableView cellForRowAtIndexPath:mIndexPath1];
            UITableViewCell *mCellPost = [mTableView cellForRowAtIndexPath:mIndexPath2];
            
            UITextField *mTextField1 = ( UITextField *)[mCellUrl viewWithTag:101];
            UITextField *mTextField2 = ( UITextField *)[mCellPost viewWithTag:102];
            mTextField1.text = DefaultURL;
            mTextField2.text = DefaultPost;
        }
        return;
    }
    if (mSetType == 2) {
        if ([indexPath row] == 0 && [indexPath section] == 0) {
            ViewController *mViewController = [[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
            
            [UIApplication sharedApplication].keyWindow.rootViewController = mViewController;
            [mViewController release];
        }
        if ([indexPath row] == 1 && [indexPath section] == 0) {
            NSString *title = UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation) ? @"\n\n" : @"\n\n" ;
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"修改密码" message:title delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"发送", nil];
            alertView.tag = 1001;
            alertView.delegate = self;
            [alertView show];
            [alertView release];
            
            UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(12, 45, 260, 30)];
            textField.tag = 10001;
            textField.delegate = self;
            textField.placeholder = @"密码";
            [textField setBorderStyle:UITextBorderStyleRoundedRect];
            [alertView addSubview:textField];
            [textField release];
            
            
        }
        if ([indexPath row] == 0 && [indexPath section] == 1) {
            NSString *title = UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation) ? @"\n\n" : @"\n\n" ;
            
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"意见反馈" message:title delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"发送", nil];
            alertView.tag = 2001;
            alertView.delegate = self;
            [alertView show];
            [alertView release];
            
            UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(12, 45, 260, 30)];
            textField.tag = 20001;
            textField.delegate = self;
            textField.placeholder = @"意见反馈";
            [textField setBorderStyle:UITextBorderStyleRoundedRect];
            [alertView addSubview:textField];
            [textField release];
        }
        if ([indexPath row] == 1 && [indexPath section] == 1) {
            mHttpRequest.Delegate = self;
            NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
            [mDict setObject:[NSNumber numberWithInt:1] forKey:@"flag"];
            NSString *string = [mDict JSONRepresentation];
            [mDict release];
            [mHttpRequest CustomFormDataRequestDict:string tag:VersionTag url:[InstanceView FormatUrl:@"ClientFetchVersion"]];
        }
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{

    if (alertView.tag == 1001) {
        if ([((UITextField*)[alertView viewWithTag:10001]).text length]==0 ) {
            
            UIAlertView *mAlertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"密码不能为空" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:nil, nil];
            mAlertView.tag = 33001;
            [mAlertView show];
            [mAlertView release];

        }
        if (buttonIndex == 1) {
            NSLog(@"发送");
            [self SendPassWord:((UITextField*)[alertView viewWithTag:10001]).text];
        }
    }
    if (alertView.tag == 2001) {
        if (buttonIndex == 1) {
            [self SendMessage:((UITextField*)[alertView viewWithTag:20001]).text];
        }
    }
    if (alertView.tag == 33001) {
        return;
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}
-(void)dealloc
{
    [mHttpRequest release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mHttpRequest = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

@end
