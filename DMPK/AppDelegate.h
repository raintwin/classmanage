//
//  AppDelegate.h
//  DMPK
//
//  Created by jiaodian on 12-10-17.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

#define NOTIFICATION_PLAN @"PLANREQUEST"
#define NOTIFICATION_BOOK @"BOOKREQUEST"

#define NOTIFICATION_QUESTPLAN @"PLANQUEST"
#define NOTIFICATION_CLOSEPLAN @"CLOSEPLAN"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
