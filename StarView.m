//
//  StarView.m
//  DMPK
//
//  Created by jiaodian on 12-11-8.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "StarView.h"

@implementation StarView
@synthesize mStarCount;
@synthesize mButtons;
@synthesize EvaluateId;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        EvaluateId = -1;
        {
            NSMutableArray *array = [[NSMutableArray alloc]init];
            self.mButtons = array;
            [array release];
        }
        mStarCount  = 5;
        for (int i = 0; i<5; i++) {
            UIButton *mButton = [[UIButton alloc]initWithFrame:CGRectMake(i*25, 4, 22, 21)];
            [mButton setBackgroundImage:[UIImage imageNamed:@"星星点亮后.png"] forState:0];
            mButton.tag = i;
            [mButton addTarget:self action:@selector(TouchStar:) forControlEvents:64];
            [self addSubview:mButton];
            [mButton release];
            [mButtons addObject:mButton];
        }
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
-(void)setInitData:(BOOL)IsTouch
{
    
    for (int i = 0; i<mStarCount; i++) {
        UIButton *mBTN = [mButtons objectAtIndex:i];
        [mBTN setBackgroundImage:[UIImage imageNamed:@"星星点亮后.png"] forState:0];
        [mBTN setEnabled:IsTouch];
    }
    for (int i = mStarCount; i<5; i++) {
        UIButton *mBTN = [mButtons objectAtIndex:i];
        [mBTN setBackgroundImage:[UIImage imageNamed:@"星星未点亮前.png"] forState:0];
        [mBTN setEnabled:IsTouch];
    }
}
-(void)TouchStar:sender
{
    if (mStarCount<0) {
        mStarCount = 0;
        return;
    }
    if (mStarCount>5) {
        mStarCount = 5;
        return;
    }
    UIButton *mTempButton = sender;    
    if ((mTempButton.tag+1)<= mStarCount) {
        mStarCount --;
    }
    else if ((mTempButton.tag+1)>mStarCount) {
        mStarCount ++;
    }
    for (int i = 0; i<mStarCount; i++) {
        UIButton *mBTN = [mButtons objectAtIndex:i];
        [mBTN setBackgroundImage:[UIImage imageNamed:@"星星点亮后.png"] forState:0];
    }
    for (int i = mStarCount; i<5; i++) {
        UIButton *mBTN = [mButtons objectAtIndex:i];
        [mBTN setBackgroundImage:[UIImage imageNamed:@"星星未点亮前.png"] forState:0];
    }

}
-(int)GetStarCount
{
    return mStarCount;
}
-(int)GetEvaluateId
{
    return EvaluateId;
}
-(void)dealloc
{
    [mButtons release]; mButtons = nil;
    [super dealloc];
}
@end
