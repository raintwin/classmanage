//
//  StarView.h
//  DMPK
//
//  Created by jiaodian on 12-11-8.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StarView : UIView

@property(nonatomic,assign)int mStarCount,EvaluateId;
@property(nonatomic,retain)NSMutableArray *mButtons;

-(void)setInitData:(BOOL)IsTouch;
-(int)GetEvaluateId;
-(int)GetStarCount;
@end
