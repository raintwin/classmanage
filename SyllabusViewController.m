//
//  SyllabusViewController.m
//  DMPK
//
//  Created by jiaodian on 12-10-30.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "SyllabusViewController.h"
#import "SyllabusScrollView.h"
#import "InstanceView.h"
#import "MainViewCtrl.h"
#define TableviewHeight 43
@interface SyllabusViewController ()

@end

@implementation SyllabusViewController
@synthesize mTeacherId;
@synthesize mDataArray;
@synthesize mDataDict;
@synthesize mHttpRequest;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        mTeacherId = -1;
        mDays = -1;
    }
    return self;
}
-(void)CalculateDate:(NSMutableDictionary*)mDict
{
    NSDate *startDate;
    NSDate *endDate;

    {
        NSDateComponents *comps = [[NSDateComponents alloc] init];            
        [comps setDay:[InstanceView FormatDateOfeDay:[mDict objectForKey:@"startDate"]]];
        [comps setMonth:[InstanceView FormatDateOfeMonths:[mDict objectForKey:@"startDate"]]];    
        [comps setYear:[InstanceView FormatDateOfeYear:[mDict objectForKey:@"startDate"]]]; 
        
        NSCalendar *start = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];    
        
        startDate= [start dateFromComponents:comps];;    
        

        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"ww"];
        NSString *locatinString = [formatter stringFromDate:startDate];
        [formatter release];
        NSLog(@"locatinString:%@",locatinString);
        [comps release];
        [start release];
    }
    {
        NSDateComponents *comps = [[NSDateComponents alloc] init];    
        
        [comps setDay:[InstanceView FormatDateOfeDay:[mDict objectForKey:@"date"]]];    
        [comps setMonth:[InstanceView FormatDateOfeMonths:[mDict objectForKey:@"date"]]];    
        [comps setMonth:10];    

        [comps setYear:[InstanceView FormatDateOfeYear:[mDict objectForKey:@"date"]]]; 
        
        NSCalendar *end = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];    
        
        endDate= [end dateFromComponents:comps];;    
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"ww"];
        NSString *locatinString = [formatter stringFromDate:endDate];
        [formatter release];
        NSLog(@"locatinString:%@",locatinString);

        [comps release];
        [end release];
    }
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];    
    
    NSUInteger unitFlags = NSMonthCalendarUnit | NSDayCalendarUnit;    
    
    NSDateComponents *components = [gregorian components:unitFlags fromDate:startDate toDate:endDate options:0];    
    
    NSInteger weekday = [components weekday]; 
    NSInteger days = [components day]; 
    
    [gregorian release];
    NSLog(@"calculate days %d",days);
    NSLog(@"calculate weekday %d",weekday);
    mDays = days;
    
    
    
}
-(NSString*)CalculateDateOfWeek:(NSString*)DateString
{
    NSDateComponents *comps = [[NSDateComponents alloc] init];    
    [comps setDay:[InstanceView FormatDateOfeDay:DateString]];    
    [comps setMonth:[InstanceView FormatDateOfeMonths:DateString]];    
    [comps setYear:[InstanceView FormatDateOfeYear:DateString]]; 
    
    NSCalendar *Calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];    
    
    NSDate *endDate = [Calendar dateFromComponents:comps];;    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"ww"];
    NSString *locatinString = [formatter stringFromDate:endDate];
    
    [formatter release];
    [Calendar release];
    [comps release];
    NSLog(@"locatinString:%@",locatinString);
    return locatinString;
}
-(void)JudgeSingleWeek:(int)startDate endDate:(int)endData
{
    int count = endData - startDate;
    if (count<0) {
        return;
    }
    if (count %2 == 0) {
        isSingleWeek = 1;
    }
    else {
        isSingleWeek = 2;
    }
}
#pragma mark 历史课堂
-(void)HistoryClass
{
    if ([mDataArray count] == 0) {
        return;
    }
    NSLog(@"历史课堂 -mDataDict %@",[mDataDict JSONRepresentation]);
    
    SearchClassroomViewController *mSearchClassroom = [[SearchClassroomViewController alloc]initWithNibName:@"SearchClassroomViewController" bundle:nil];
    mSearchClassroom.IsSyllabus = YES;
    mSearchClassroom.mDataDict = mDataDict;
    [self.navigationController pushViewController:mSearchClassroom animated:YES];
    [mSearchClassroom release];
    
    
    
}
-(void)GetChooseClassroomPlan:(int)ClassId
{

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSLog(@"Teacher ID -:%d",mTeacherId);
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"座位表大背景.png"]]];

    {
        NSMutableArray *array =[[NSMutableArray alloc]init];
        self.mDataArray = array;
        [array release];
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        self.mDataDict = mDict;
        [mDict release];
    }
    
    
    {
        HttpFormatRequest *http = [[HttpFormatRequest alloc]init];
        http.Delegate = self;
        self.mHttpRequest = http;
        [http release];
        
        [self.view addSubview:mHttpRequest];
        
        
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:[NSNumber numberWithInt:mTeacherId] forKey:@"teacherId"];
        NSString *string= [mDict JSONRepresentation];
        [mDict release];
        
        [mHttpRequest CustomFormDataRequestDict:string tag:0 url:[InstanceView FormatUrl:@"ClientScheduleReserve"]];
    }
    
    
    {
        UITableView *mTableView = [[UITableView alloc]initWithFrame:CGRectMake(28, 114, 251, 615) style:UITableViewStylePlain];
        [mTableView setBackgroundColor:[UIColor clearColor]];
        mTableView.delegate = self;
        mTableView.dataSource = self;
        mTableView.tag = 10101;
        [self.view addSubview:mTableView];
        [mTableView release];
    }
    
    
    {
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"历史课堂250.png" point:CGPointMake(29, 475+43*4)];
        [mButton addTarget:self action:@selector(HistoryClass) forControlEvents:64];
    }
    
    [mTeatherInfoDict setObject:[NSNumber numberWithInt:2] forKey:WEBINFOSTRING];

}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    NSLog(@"tag = %d",tag);
    
    if (tag == BACKRESULTCODE) {
        
        return;
    }
    if (tag-BACKRESULTCODE == 0) {
        return;
    }
    int maxsectionId=0;
    
    [mDataDict setObject:[[ResultContent JSONValue] objectForKey:@"startDate"] forKey:@"startDate"];
    [mDataDict setObject:[[ResultContent JSONValue] objectForKey:@"termName"] forKey:@"termName"];
    [mDataDict setObject:[[ResultContent JSONValue] objectForKey:@"date"] forKey:@"date"];
    [mDataDict setObject:[[ResultContent JSONValue] objectForKey:@"teacherId"] forKey:@"teacherId"];
    [mDataDict setObject:[[ResultContent JSONValue] objectForKey:@"termId"] forKey:@"termId"];
    
    [self JudgeSingleWeek:[[self CalculateDateOfWeek:[[ResultContent JSONValue] objectForKey:@"startDate"]] intValue] endDate:[[self CalculateDateOfWeek:[[ResultContent JSONValue] objectForKey:@"date"]]intValue]];
    
    
    NSLog(@"mDataDict :%@",[mDataDict JSONRepresentation]);


    
    NSArray *array = [[[ResultContent JSONValue] valueForKey:@"schedule"] retain];
    for (int i = 0; i<[array count]; i++) {
        
        InstanceObject *mInstanceObject = [[InstanceObject alloc]init];
        mInstanceObject.gradeId = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"gradeId"]] intValue];
        mInstanceObject.sectionId = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"sectionId"]] intValue];
        mInstanceObject.subjectId = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"subjectId"]] intValue];
        mInstanceObject.weekId = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"weekId"]] intValue];
        mInstanceObject.seniorId = [[NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"seniorId"]] intValue];

        
        mInstanceObject.gradeName = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"gradeName"]];
        mInstanceObject.isDefault = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"isDefault"]]; 
        mInstanceObject.isSingleWeek = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"isSingleWeek"]]; 
        mInstanceObject.subjectName = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"subjectName"]]; 
        mInstanceObject.adjDate = [NSString stringWithFormat:@"%@",[[array objectAtIndex:i] valueForKey:@"adjDate"]]; 
        
        
        [mDataDict setObject:[[array objectAtIndex:i] valueForKey:@"gradeId"] forKey:@"gradeId"];

        if (maxsectionId<mInstanceObject.sectionId) {
            maxsectionId = mInstanceObject.sectionId;
        }
        
        if (isSingleWeek == [mInstanceObject.isSingleWeek intValue] || [mInstanceObject.isSingleWeek intValue] == 0 ) {
            if ([mInstanceObject.isDefault intValue] == 1) {
                [mDataArray addObject:mInstanceObject];
            }
            else if ([mInstanceObject.isDefault intValue] == 0) {
                NSString *Date = [[ResultContent JSONValue] objectForKey:@"date"];
                
                if ([[self CalculateDateOfWeek:Date] isEqualToString:[self CalculateDateOfWeek:mInstanceObject.adjDate]]) {
                    [mDataArray addObject:mInstanceObject];
                }
            }
        }
        [mInstanceObject release];
        
    }
    [array release];
    [((UITableView*)[self.view viewWithTag:10101]) reloadData];
    

    [InstanceView addLabelOfViewRect:self.view rect:CGRectMake(330, 53, 666, 40) string:[[ResultContent JSONValue] objectForKey:@"termName"] textSize:16 textAlignment:UITextAlignmentCenter];

    
    SyllabusScrollView *mSyllabusScrollView = [[SyllabusScrollView alloc]initWithFrame:CGRectMake(322, 223, 639, 375)];
    mSyllabusScrollView.delegeta = self;
    [mSyllabusScrollView setContentSize:CGSizeMake(0, maxsectionId*39)];
    mSyllabusScrollView.maxsectionId = maxsectionId;
    [mSyllabusScrollView initData:mDataArray];
    [self.view addSubview:mSyllabusScrollView];
    [mSyllabusScrollView release];
}
#pragma mark 选择课堂
-(void)ResponseScroller:(int)tag
{
    InstanceObject *mInstanceObject = [mDataArray objectAtIndex:tag];
    
    MainViewCtrl *mMainViewCtrl = [[MainViewCtrl alloc]initWithNibName:@"MainViewCtrl" bundle:nil];
    mMainViewCtrl.mInstanceObject = mInstanceObject;
    mMainViewCtrl.mDataDict = mDataDict;
    [self.navigationController pushViewController:mMainViewCtrl animated:YES];
    [mMainViewCtrl release];
} 
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
    
}
-(void)PCColorDefault:(UIView*)view  colorNumber:(int)ColorNmuber section:(int)Section
{
    
    switch (ColorNmuber%5) {
        case 0:
            [view setBackgroundColor:[UIColor colorWithRed:0.0 green:153/255.0 blue:204/255.0 alpha:1.0]];
            break;
        case 1:
            [view setBackgroundColor:[UIColor colorWithRed:153/255.0 green:204/255.0 blue:51/255.0 alpha:1.0]];
            break;
        case 2:
            [view setBackgroundColor:[UIColor colorWithRed:1.0 green:153/255.0 blue:51/255.0 alpha:1.0]];
            break;
        case 3:
            [view setBackgroundColor:[UIColor colorWithRed:1.0 green:51/255.0 blue:51/255.0 alpha:1.0]];
            break;
        case 4:
            [view setBackgroundColor:[UIColor colorWithRed:1.0 green:220/255.0 blue:0.0 alpha:1.0]];
            break;
          
        default:
            break;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *cellName = @"cellstr";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    
    InstanceObject *mInstanceObject = [mDataArray objectAtIndex:[indexPath row]];

    if (cell== nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
        cell.textLabel.text = @"";
//        [self PCColorDefault:cell.contentView colorNumber:mInstanceObject.weekId section:mInstanceObject.sectionId];
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 250, 43)];
        imageView.image = nil;
        imageView.tag = 100111;
        [cell.contentView insertSubview:imageView atIndex:0];
        [imageView release];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    ((UIImageView *)[cell.contentView viewWithTag:100111]).image = nil;
    NSString *ImgFile = [NSString stringWithFormat:@"left_0%d.png",([indexPath row]+1)%7];
    ((UIImageView *)[cell.contentView viewWithTag:100111]).image = [UIImage imageNamed:ImgFile];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@ %@ %@",
                           [InstanceView FormatWeek:mInstanceObject.weekId],[InstanceView FormatSection:mInstanceObject.sectionId],mInstanceObject.gradeName,mInstanceObject.subjectName  ];
    
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
    cell.textLabel.textAlignment = UITextAlignmentCenter;
    [cell.textLabel setTextColor:[UIColor whiteColor]];
//    [self PCColorDefault:cell.contentView colorNumber:mInstanceObject.weekId section:mInstanceObject.sectionId];

    
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return [mDataArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TableviewHeight;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}
-(void)dealloc
{
    [mDataArray release];
    [mDataDict release];
    [mHttpRequest release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mDataArray = nil;
    self.mDataDict = nil;
    self.mHttpRequest = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
