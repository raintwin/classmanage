//
//  TeacherSaveEvaluateController.m
//  DMPK
//
//  Created by jiaodian on 12-11-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "TeacherSaveEvaluateController.h"
#import "InstanceObject.h"
#define TableviewHeight 29

#define ClientTeacherEvaluate 1212201
#define ClientTeacherSaveEvaluate 1212202

@interface TeacherSaveEvaluateController ()

@end

@implementation TeacherSaveEvaluateController
@synthesize evaluateMainId;
@synthesize mEvaluateDetails;
@synthesize mDataDict;
@synthesize mStarViewItems;
@synthesize mInfoDict;
@synthesize mHttpRequest;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        evaluateMainId = 0 ;
        mItemId = 0;
    }
    return self;
}
#pragma mark 返回
-(void)SetBackNav
{
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [InstanceView addUIImageViewOfPoint:self.view imageFile:@"学生的评价xx.png" point:CGPointMake(0,0)];
    mItemId = 0;
    {
        NSMutableArray *array = [[NSMutableArray alloc]init];
        self.mEvaluateDetails = array;
        [array release];
    }
    {
        NSMutableArray *array = [[NSMutableArray alloc]init];
        self.mStarViewItems = array;
        [array release];
    }
    {
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        self.mInfoDict = mDict;
        [mDict release];
    }

    {
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"返回.png" point:CGPointMake(29, 10)];
        [mButton addTarget:self action:@selector(SetBackNav) forControlEvents:64];
    }
    {
        HttpFormatRequest *http = [[HttpFormatRequest alloc]init];
        http.Delegate = self;
        self.mHttpRequest = http;
        [http release];
        [self.view addSubview:mHttpRequest];

        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:[mDataDict objectForKey:@"id"] forKey:@"id"];
        [mDict setObject:[mDataDict objectForKey:@"teacherId"] forKey:@"teacherId"];

        
        NSString *string = [mDict JSONRepresentation];
        [mDict release];
        [mHttpRequest CustomFormDataRequestDict:string tag:ClientTeacherEvaluate url:[InstanceView FormatUrl:@"ClientTeacherEvaluate"]];
    }
    
    UITableView *mTableView = [[UITableView alloc]initWithFrame:CGRectMake(104, 208, 824, 377) style:UITableViewStylePlain];
    [mTableView setBackgroundColor:[UIColor clearColor]];
    [mTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    mTableView.delegate = self;
    mTableView.dataSource  = self;
    mTableView.tag = 100111;
    [self.view addSubview:mTableView];
    [mTableView release];
    
    [InstanceView addUIImageViewOfPoint:self.view imageFile:@"学生的评价xx线.png" point:CGPointMake(104,585)];

    
    

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];  
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];  

}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    if (tag == ClientTeacherSaveEvaluate) {
        [((UIButton*)[self.view viewWithTag:414001]) removeFromSuperview];
        return;
    }
    [mInfoDict setObject:[[ResultContent JSONValue] objectForKey:@"id"] forKey:@"id"];
    [mInfoDict setObject:[[ResultContent JSONValue] objectForKey:@"teacherId"] forKey:@"teacherId"];
    [mInfoDict setObject:[[ResultContent JSONValue] objectForKey:@"comment"] forKey:@"comment"];
    [mInfoDict setObject:[[ResultContent JSONValue] objectForKey:@"flag"] forKey:@"flag"];

    NSMutableArray *tmparry;
    
    NSArray *students = [[[ResultContent JSONValue] valueForKey:@"evaluate"] retain];
    for (int i = 0; i<[students count]; i++) {
        StudentEvaluateDetail *mStudentEvaluateDetail = [[StudentEvaluateDetail alloc]init];
        mStudentEvaluateDetail.itemId  = [[NSString stringWithFormat:@"%@",[[students objectAtIndex:i] valueForKey:@"itemId"]] intValue];
        mStudentEvaluateDetail.evaluateId  = [[NSString stringWithFormat:@"%@",[[students objectAtIndex:i] valueForKey:@"evaluateId"]] intValue];
        mStudentEvaluateDetail.evaluateScore = [[NSString stringWithFormat:@"%@",[[students objectAtIndex:i] valueForKey:@"evaluateScore"]] intValue];
        mStudentEvaluateDetail.score = [[NSString stringWithFormat:@"%@",[[students objectAtIndex:i] valueForKey:@"score"]] intValue];
        
        mStudentEvaluateDetail.itemDescription  = [NSString stringWithFormat:@"%@",[[students objectAtIndex:i] valueForKey:@"itemDescription"]];
        mStudentEvaluateDetail.description = [NSString stringWithFormat:@"%@",[[students objectAtIndex:i] valueForKey:@"description"]];
        if (i == 0) {
            mItemId = mStudentEvaluateDetail.itemId;
            tmparry = [[NSMutableArray alloc]init];
            
        }
        
        if (mItemId == mStudentEvaluateDetail.itemId) {
            [tmparry addObject:mStudentEvaluateDetail];
            [mStudentEvaluateDetail release];
        }
        if (mItemId != mStudentEvaluateDetail.itemId) {
            [mEvaluateDetails addObject:tmparry];
            [tmparry release];
            
            mItemId = mStudentEvaluateDetail.itemId; 
            tmparry = [[NSMutableArray alloc]init];
            [tmparry addObject:mStudentEvaluateDetail];
            [mStudentEvaluateDetail release];
        }
        if (i == [students count]-1) {
            [mEvaluateDetails addObject:tmparry];
            [tmparry release];
        }
        
    }
    [students release];
    [((UITableView *)[self.view viewWithTag:100111]) reloadData];

    
    
    UIPlaceHolderTextView *mPlaceHolderTextView = [[UIPlaceHolderTextView alloc]initWithFrame:CGRectMake(104, 586, 824, 110)];
    [mPlaceHolderTextView setReturnKeyType:UIReturnKeyNext];
    [mPlaceHolderTextView setBackgroundColor:[UIColor clearColor]];
    if ([[[ResultContent JSONValue] objectForKey:@"comment"] length] == 0) {
        [mPlaceHolderTextView setPlaceholder:@"总评语:"];
    }
    else {
        mPlaceHolderTextView.text = [[ResultContent JSONValue] objectForKey:@"comment"];
    }
    mPlaceHolderTextView.delegate = self;
    mPlaceHolderTextView.tag = 220011;
    [self.view addSubview:mPlaceHolderTextView];
    if ([[[ResultContent JSONValue] objectForKey:@"flag"] boolValue]) {
        [mPlaceHolderTextView setEditable:NO];
    }
    else {
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"保存.png" point:CGPointMake(104, 120)];
        mButton.tag = 414001;
        [mButton addTarget:self action:@selector(SavaTeacherEvaluate) forControlEvents:64];

    }
    [mPlaceHolderTextView release];

    
}
-(void)GetTeaherScore
{
    for (int i = 0; i<[mEvaluateDetails count]; i++) {
        NSMutableArray *tempArray = [mEvaluateDetails objectAtIndex:i];
        for (StudentEvaluateDetail *mDetail in tempArray) {
            for (StarView *mStarView in mStarViewItems) {
                if (mDetail.evaluateId == [mStarView GetEvaluateId]) {
                    mDetail.score = [mStarView GetStarCount];
                }
            }
        }
    }
}
-(void)SavaTeacherEvaluate
{
    [self GetTeaherScore];
    NSMutableArray *mArray = [[NSMutableArray alloc]init];
    for (int i = 0; i<[mEvaluateDetails count]; i++) {
        NSMutableArray *tempArray = [mEvaluateDetails objectAtIndex:i];
        for (StudentEvaluateDetail *mDetail in tempArray) {
            NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
            [mDict setObject:[NSNumber numberWithInt:mDetail.score] forKey:@"score"];
            [mDict setObject:[NSNumber numberWithInt:mDetail.evaluateId] forKey:@"evaluateId"];
            [mArray addObject:mDict];
            [mDict release];

        }
    }
    NSString *comment = ((UIPlaceHolderTextView*)[self.view viewWithTag:220011]).text;
    NSLog(@"mArray :%@",[mArray JSONRepresentation]);
    {
        mHttpRequest.Delegate = self;
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:[mDataDict objectForKey:@"id"] forKey:@"id"];
        [mDict setObject:[mDataDict objectForKey:@"teacherId"] forKey:@"teacherId"];
        [mDict setObject:comment forKey:@"comment"];

        [mDict setObject:mArray forKey:@"evaluate"];
        
        [mArray release];
        
        
        
        NSString *string = [mDict JSONRepresentation];
        
        [mDict release];
        [mHttpRequest CustomFormDataRequestDict:string tag:ClientTeacherSaveEvaluate url:[InstanceView FormatUrl:@"ClientTeacherSaveEvaluate"]];
    }

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *cellName = @"cellstr";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if (cell== nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
        cell.textLabel.text = @"";
    }
    for (UIView *sub in [cell.contentView subviews]) {
        [sub removeFromSuperview];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    NSMutableArray *tempArray = [mEvaluateDetails objectAtIndex:[indexPath row]];
    int textSize = 14;
    BOOL isflag = [[mInfoDict objectForKey:@"flag"] boolValue];
    for (int i = 0; i<[tempArray count]; i++) {
        int cellY = i*TableviewHeight;
        StudentEvaluateDetail *mDetail = [tempArray objectAtIndex:i];
        [InstanceView addUIImageView:cell.contentView imageFile:@"学生的评价xx行.png" rect:CGRectMake(98,cellY,726,29)];
        if (i == 0) {
            [InstanceView addLabelOfViewRect:cell.contentView rect:CGRectMake(0, 0, 100, TableviewHeight) string:mDetail.itemDescription textSize:textSize textAlignment:UITextAlignmentCenter];
        }
        
        [InstanceView addLabelOfViewRect:cell.contentView rect:CGRectMake(100, cellY, 450, TableviewHeight) string:mDetail.description textSize:textSize textAlignment:UITextAlignmentCenter];
        
        [InstanceView addLabelOfViewRect:cell.contentView rect:CGRectMake(550, cellY, 90, TableviewHeight) string:[NSString stringWithFormat:@"%d",mDetail.evaluateScore] textSize:textSize textAlignment:UITextAlignmentCenter];
        
        StarView *mStarView = [[StarView alloc]initWithFrame:CGRectMake(650, cellY, 180,TableviewHeight)];

        mStarView.mStarCount = mDetail.score;
        if (!isflag) {
            mStarView.mStarCount = 5;
        }
        [mStarView setEvaluateId:mDetail.evaluateId];
        [mStarView  setInitData:!isflag];
        
        [mStarViewItems addObject:mStarView];
        [cell.contentView addSubview:mStarView];
        [mStarView release];
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    
    return [mEvaluateDetails count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *tmepArray = [mEvaluateDetails objectAtIndex:[indexPath row]];
    return [tmepArray count]*TableviewHeight;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    PlanObject *mPlanObject = [mPlans objectAtIndex:[indexPath row]];
    //    [delegate GetChooseClassroomPlan:mPlanObject.planId];
    //    [self.navigationController popViewControllerAnimated:YES];
}


-(void)MoveSelfView:(BOOL)ShowKeyBoard
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3f];
    CGRect rect = self.view.frame;
    if (ShowKeyBoard) {
        rect.origin.y = -330;
        [self.view setFrame:rect];
    }
    else {
        rect.origin.y = 0;
        [self.view setFrame:rect];
    }
    [UIView commitAnimations];
}

- (void)keyboardWillShow:(NSNotification *)notification {  
    [self MoveSelfView:YES];
}
- (void)keyboardWillHide:(NSNotification *)notification {  
    [self MoveSelfView:NO];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField;              // called when 'return' key pressed. return NO to ignore.
{
    [self MoveSelfView:NO];
    [textField resignFirstResponder];
    return YES;
}


-(void)dealloc
{
    [mDataDict release];
    [mEvaluateDetails release];
    [mStarViewItems release];
    [mInfoDict release];
    [mHttpRequest release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mEvaluateDetails = nil;
    self.mDataDict = nil;
    self.mStarViewItems = nil;
    self.mInfoDict = nil;
    self.mHttpRequest = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
