//
//  planViewController.m
//  DMPK
//
//  Created by applebaba on 13-11-11.
//
//

#import "planViewController.h"
#import "InstanceView.h"
#import "InstanceObject.h"
#import "AppDelegate.h"


static NSString *const ClientSeniorRequest = @"ClientSenior";     //年级
static NSString *const ClientVersionRequest = @"ClientVersion";   //版本
static NSString *const ClientSubjectRequest = @"ClientSubject";   //科目

static NSString *const ClientCatalogRequest = @"ClientCatalog";   //课本 目录大类

static NSString *const TextCatalogRequest = @"TextCatalog";         //课本 目录一
static NSString *const TextPlanCatalogRequest = @"TextPlanCatalog"; //课本 目录二
static NSString *const TextPlanCatalogDataRequest = @"TextPlanCatalogData"; //课本 目录三

static NSString *const ClientPlanRequest = @"ClientPlan"; // 教案列表

static NSString *const DirectoryDataRequest = @"directoryDatas";



@implementation DirectoryData
@synthesize did,date,day,hours,minutes,month,nanos,seconds,time,timezoneOffset,year,pid,seniorId,subjectId,type,versionId;
@synthesize createBy,name;
@end

@interface planViewController ()
@property(nonatomic,retain) NSMutableArray *array,*directoryDetails;
@property(nonatomic,retain) HttpFormatRequest *httpRequest;
@end

@implementation planViewController
@synthesize array,directoryDatas,directoryDetails;
@synthesize cleintRequest,titleString;
@synthesize httpRequest;
@synthesize backButton;
@synthesize titleLabel,tipLabel;
@synthesize versionId ,subjectId ,seniorId ,pid,catalogId;
@synthesize isBook;
-(void)dealloc
{
    [super dealloc];
    [array release];
    [directoryDetails release];
    [directoryDatas release];
    
}
-(void)viewDidUnload
{
    [super viewDidUnload];
    self.array = nil;
    self.directoryDetails = nil;
    self.directoryDatas = nil;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.tipLabel.hidden = YES;
    NSLog(@"titleString = %@,",titleString);
    self.titleLabel.text = titleString;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;

    self.httpRequest = [[HttpFormatRequest alloc]init];
    self.httpRequest.Delegate = self;
    [self.view addSubview:self.httpRequest];
    if ([self.cleintRequest isEqualToString:ClientVersionRequest]    ) {
        [self.httpRequest CustomFormDataRequestDict:@"" tag:0 url:[InstanceView FormatUrl:ClientVersionRequest]];

//        self.backButton.hidden = YES;
    }  else  if ([self.cleintRequest isEqualToString:ClientCatalogRequest]) {
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:[NSNumber numberWithInt:self.subjectId] forKey:@"subjectId"];
        [mDict setObject:[NSNumber numberWithInt:self.versionId] forKey:@"versionId"];
//        [mDict setObject:[NSNumber numberWithInt:self.seniorId] forKey:@"seniorId"];
        [self.httpRequest CustomFormDataRequestDict:[mDict JSONRepresentation] tag:0 url:[InstanceView FormatUrl:self.cleintRequest]];
        [mDict release];
    } else if([self.cleintRequest isEqualToString:TextCatalogRequest] || [self.cleintRequest isEqualToString:TextPlanCatalogRequest]){
        [self setDirectoryDetailData];
        NSLog(@"self.directoryDetails = %d",[self.directoryDetails count]);
    }else  if ([self.cleintRequest isEqualToString:ClientPlanRequest]) {
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:[NSNumber numberWithInt:self.catalogId] forKey:@"catalogId"];
        [self.httpRequest CustomFormDataRequestDict:[mDict JSONRepresentation] tag:0 url:[InstanceView FormatUrl:self.cleintRequest]];
        [mDict release];
    }

    else {
        [self.httpRequest CustomFormDataRequestDict:@"" tag:0 url:[InstanceView FormatUrl:self.cleintRequest]];

    }
    
    NSLog(@"self.cleintRequest = %@",self.cleintRequest);
    NSLog(@"self.seniorId = %d  self.subjectId = %d, self.versionId =  %d",self.seniorId,self.subjectId,self.versionId);

}
- (IBAction)backButton:(id)sender;
{
    if ([self.cleintRequest isEqualToString:ClientVersionRequest])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CLOSEPLAN object:self];
        return;
    }
    [self.navigationController popViewControllerAnimated:NO];
}
-(void)addPlans:(NSArray *)Plans tag:(int)tag;
{
    NSMutableArray *datas = [[NSMutableArray alloc]init];

    if ([self.cleintRequest isEqualToString:ClientCatalogRequest]) {
        for (int i = 0; i<[Plans count]; i++) {
            DirectoryData *mPlanObject = [[DirectoryData alloc]init];
            mPlanObject.did = [[NSString stringWithFormat:@"%@",[[Plans objectAtIndex:i] valueForKey:@"id"]] intValue];
            mPlanObject.pid = [[NSString stringWithFormat:@"%@",[[Plans objectAtIndex:i] valueForKey:@"pid"]] intValue];
            mPlanObject.seniorId = [[NSString stringWithFormat:@"%@",[[Plans objectAtIndex:i] valueForKey:@"seniorId"]] intValue];
            mPlanObject.subjectId = [[NSString stringWithFormat:@"%@",[[Plans objectAtIndex:i] valueForKey:@"subjectId"]] intValue];
            mPlanObject.type = [[NSString stringWithFormat:@"%@",[[Plans objectAtIndex:i] valueForKey:@"type"]] intValue];
            mPlanObject.versionId = [[NSString stringWithFormat:@"%@",[[Plans objectAtIndex:i] valueForKey:@"versionId"]] intValue];

            mPlanObject.createBy = [NSString stringWithFormat:@"%@",[[Plans objectAtIndex:i] valueForKey:@"createBy"]];
            mPlanObject.name = [NSString stringWithFormat:@"%@",[[Plans objectAtIndex:i] valueForKey:@"name"]];

            
            mPlanObject.date = [[NSString stringWithFormat:@"%@",[[[Plans objectAtIndex:i] valueForKey:@"modifyDate"] valueForKey:@"date"]] intValue];
            mPlanObject.day = [[NSString stringWithFormat:@"%@",[[[Plans objectAtIndex:i] valueForKey:@"modifyDate"] valueForKey:@"day"]] intValue];
            mPlanObject.hours = [[NSString stringWithFormat:@"%@",[[[Plans objectAtIndex:i] valueForKey:@"modifyDate"] valueForKey:@"hours"]] intValue];
            mPlanObject.minutes = [[NSString stringWithFormat:@"%@",[[[Plans objectAtIndex:i] valueForKey:@"modifyDate"] valueForKey:@"minutes"]] intValue];
            mPlanObject.month = [[NSString stringWithFormat:@"%@",[[[Plans objectAtIndex:i] valueForKey:@"modifyDate"] valueForKey:@"month"]] intValue];
            mPlanObject.nanos = [[NSString stringWithFormat:@"%@",[[[Plans objectAtIndex:i] valueForKey:@"modifyDate"] valueForKey:@"nanos"]] intValue];
            mPlanObject.seconds = [[NSString stringWithFormat:@"%@",[[[Plans objectAtIndex:i] valueForKey:@"modifyDate"] valueForKey:@"seconds"]] intValue];
            mPlanObject.time = [[NSString stringWithFormat:@"%@",[[[Plans objectAtIndex:i] valueForKey:@"modifyDate"] valueForKey:@"time"]] intValue];
            mPlanObject.timezoneOffset = [[NSString stringWithFormat:@"%@",[[[Plans objectAtIndex:i] valueForKey:@"modifyDate"] valueForKey:@"timezoneOffset"]] intValue];
            mPlanObject.year = [[NSString stringWithFormat:@"%@",[[[Plans objectAtIndex:i] valueForKey:@"modifyDate"] valueForKey:@"year"]] intValue];
            [datas addObject:mPlanObject];
            [mPlanObject release];
        }
        self.directoryDatas = datas;
        [datas release];
        [self setDirectoryDetailData];
        return;
    }
    if ([self.cleintRequest isEqualToString:ClientPlanRequest]) {
        NSMutableArray *arrays = [[NSMutableArray alloc]init];
        for (int i = 0; i<[Plans count]; i++) {
            PlanObject *mPlanObject = [[PlanObject alloc]init];
            mPlanObject.planId = [[NSString stringWithFormat:@"%@",[[Plans objectAtIndex:i] valueForKey:@"planId"]] intValue];
            mPlanObject.name = [NSString stringWithFormat:@"%@",[[Plans objectAtIndex:i] valueForKey:@"name"]];
            [arrays addObject:mPlanObject];
            [mPlanObject release];
        }
        
        self.array = arrays;
        [arrays release];
        return;
    }
    
    

    
    NSMutableArray *arrays = [[NSMutableArray alloc]init];
    for (int i = 0; i<[Plans count]; i++) {
        PlanObject *mPlanObject = [[PlanObject alloc]init];
        mPlanObject.planId = [[NSString stringWithFormat:@"%@",[[Plans objectAtIndex:i] valueForKey:@"id"]] intValue];
        mPlanObject.name = [NSString stringWithFormat:@"%@",[[Plans objectAtIndex:i] valueForKey:@"name"]];
        [arrays addObject:mPlanObject];
        [mPlanObject release];
    }

    self.array = arrays;
    [arrays release];
    
    NSLog(@"self.planSeniors  count = %d",[self.array count]);
}
-(void)setDirectoryDetailData
{
    [self.directoryDetails removeAllObjects];
    self.directoryDetails = nil;
    self.directoryDetails = [[NSMutableArray alloc]init];
    for (DirectoryData *mPlanObject in self.directoryDatas) {
        if (mPlanObject.pid == self.pid) {
            [self.directoryDetails addObject:mPlanObject];
            NSLog(@"name = %@",mPlanObject.name);
        }

    }
    if ([self.directoryDetails count] == 0) {
        self.tipLabel.hidden = NO;

    }
    [self.tableView reloadData];

}
#pragma mark - UIGestureRecognizerDelegate
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{

    if (tag != 0) {
        NSLog(@"没有数据");
        self.tipLabel.hidden = NO;

        return;
    }

    if ([self.cleintRequest isEqualToString:ClientPlanRequest]) {
        [self addPlans:[[[ResultContent JSONValue] objectForKey:@"plan"] retain] tag:tag];
    }
    else
    [self addPlans:[[ResultContent JSONValue] retain] tag:tag];
    
    [self.tableView reloadData];
    
}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
    self.tipLabel.hidden = NO;

    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"网络错误" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    alert.tag = tag;
    alert.delegate = self;
    [alert show];
    [alert release];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *cellName = @"cellstr";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if (cell== nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
        cell.textLabel.text = @"";
        [InstanceView addUIImageViewOfPoint:cell.contentView imageFile:@"planCellBack.png" point:CGPointMake(0,0)];
        UILabel *label = [InstanceView addLabelOfViewRect:cell.contentView rect:CGRectMake(0, 0, 500, 32) string:@"" textSize:14 textAlignment:UITextAlignmentCenter];
        label.tag = 20011;

        [cell bringSubviewToFront:cell.textLabel];
    }

    if ([cleintRequest isEqualToString:ClientCatalogRequest] || [cleintRequest isEqualToString:TextCatalogRequest] || [self.cleintRequest isEqualToString:TextPlanCatalogRequest]) {

        DirectoryData *directoryData = [self.directoryDetails objectAtIndex:[indexPath row]];
//        cell.textLabel.text = directoryData.name;
        ((UILabel*)[cell.contentView viewWithTag:20011]).text = directoryData.name;

        return cell;
    }
    PlanObject *mPlanObject = nil;
    mPlanObject = [self.array objectAtIndex:[indexPath row]];

//    cell.textLabel.text = mPlanObject.name;
    ((UILabel*)[cell.contentView viewWithTag:20011]).text = mPlanObject.name;

    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if ([cleintRequest isEqualToString:ClientCatalogRequest] || [cleintRequest isEqualToString:TextCatalogRequest] || [self.cleintRequest isEqualToString:TextPlanCatalogRequest]) {
        return [self.directoryDetails count];
    }
    return [self.array count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 32;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    PlanObject *mPlanObject = nil;
    
    
    planViewController *planViewCrtl = [[planViewController alloc]initWithNibName:@"planViewController" bundle:nil];
    NSLog(@"mPlanObject.planId = %d",mPlanObject.planId);
    if ([self.cleintRequest isEqualToString:ClientSeniorRequest]) {
        mPlanObject = [self.array objectAtIndex:[indexPath row]];
        planViewCrtl.cleintRequest = ClientSubjectRequest;
        planViewCrtl.seniorId = mPlanObject.planId;
        planViewCrtl.isBook = self.isBook;
    }else if([self.cleintRequest isEqualToString:ClientSubjectRequest])
    {
        mPlanObject = [self.array objectAtIndex:[indexPath row]];
        planViewCrtl.cleintRequest = ClientVersionRequest;
        planViewCrtl.seniorId = self.seniorId;
        planViewCrtl.subjectId = mPlanObject.planId;
        planViewCrtl.isBook = self.isBook;

    }
    else if([self.cleintRequest isEqualToString:ClientVersionRequest]){ //选择版本
        mPlanObject = [self.array objectAtIndex:[indexPath row]];
        planViewCrtl.titleString = @"请选择分册";
        planViewCrtl.cleintRequest = ClientCatalogRequest;
        planViewCrtl.seniorId = self.seniorId;
        planViewCrtl.subjectId = self.subjectId;
        planViewCrtl.versionId = mPlanObject.planId;
        planViewCrtl.isBook = self.isBook;

        
    }
    else if([self.cleintRequest isEqualToString:ClientCatalogRequest] ){
        DirectoryData *directoryData = [self.directoryDetails objectAtIndex:[indexPath row]];
        planViewCrtl.titleString = @"请选择目录";

        planViewCrtl.directoryDatas = self.directoryDatas;
        planViewCrtl.pid = directoryData.did;
        planViewCrtl.cleintRequest = TextCatalogRequest;
        planViewCrtl.isBook = self.isBook;

    }
    else if([self.cleintRequest isEqualToString:TextCatalogRequest]){
        planViewCrtl.titleString = @"请选择课本";

        DirectoryData *directoryData = [self.directoryDetails objectAtIndex:[indexPath row]];
        planViewCrtl.directoryDatas = self.directoryDatas;
        planViewCrtl.pid = directoryData.did;
        planViewCrtl.cleintRequest = TextPlanCatalogRequest;
        planViewCrtl.isBook = self.isBook;

    } else if([self.cleintRequest isEqualToString:TextPlanCatalogRequest]){
        planViewCrtl.titleString = @"请选择教案";

        DirectoryData *directoryData = [self.directoryDetails objectAtIndex:[indexPath row]];
        planViewCrtl.cleintRequest = ClientPlanRequest;
        planViewCrtl.catalogId = directoryData.did;
        
        if (self.isBook) {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
            [dict setObject:[NSNumber numberWithInt:directoryData.did] forKey:@"catalogId"];
            [dict setObject:[NSNumber numberWithInt:mPlanObject.planId] forKey:@"planId"];
            [dict setObject:[NSNumber numberWithBool:YES] forKey:@"isBook"];

            NSLog(@"dict = %@",dict);
            [[NSUserDefaults standardUserDefaults] setObject:dict forKey:NOTIFICATION_QUESTPLAN];

            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_BOOK object:self];
            return;
        }
        
    } else if([self.cleintRequest isEqualToString:ClientPlanRequest]){
        mPlanObject = [self.array objectAtIndex:[indexPath row]];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setObject:[NSNumber numberWithInt:self.catalogId] forKey:@"catalogId"];
        [dict setObject:[NSNumber numberWithInt:mPlanObject.planId] forKey:@"planId"];
        [dict setObject:[NSNumber numberWithBool:NO] forKey:@"isBook"];

        NSLog(@"dict = %@",dict);
        [[NSUserDefaults standardUserDefaults] setObject:dict forKey:NOTIFICATION_QUESTPLAN];

        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_PLAN object:self];
        return;
    }
    

    [self.navigationController pushViewController:planViewCrtl animated:NO];
}

@end
