//
//  InstanceView.m
//  DMPK
//
//  Created by jiaodian on 12-10-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "InstanceView.h"

@implementation InstanceView
#pragma mark  ---------------- UIButton
+(UIButton *)addUIButtonByImageAndAutoSize:(UIView *)fview image:(NSString*)imgFile  point:(CGPoint)point;
{	
    UIImage *image = [UIImage imageNamed:imgFile];
    CGRect rect = CGRectMake(point.x,point.y,image.size.width,image.size.height);
	UIButton *btn=[[UIButton alloc] initWithFrame:rect];
    [btn setBackgroundImage:image forState:0];
	[fview addSubview:btn];
	[btn release];
	return	btn;
}

+(UIButton *)addUIButtonByImage:(UIView *)fview image:(NSString*)imgFile  rect:(CGRect)rect;
{	
    UIImage *image = [UIImage imageNamed:imgFile];
	UIButton *btn=[[UIButton alloc] initWithFrame:rect];
    [btn setBackgroundImage:image forState:0];
	[fview addSubview:btn];
	[btn release];
	return	btn;
}
//+(UIButton *)addUIButtonByImage:(UIView *)fview image:(NSString*)imgFile  rect:(CGRect)rect;
//{	
//    UIImage *image = [UIImage imageNamed:imgFile];
//	UIButton *btn=[[UIButton alloc] initWithFrame:rect];
//    [btn setBackgroundImage:image forState:0];
//	[fview addSubview:btn];
//	[btn release];
//	return	btn;
//}
#pragma mark imageview
+(UIImageView*)addUIImageView:(UIView*)tmpeView imageFile:(NSString*)imgFile rect:(CGRect)rect
{
    UIImage *image = [UIImage imageNamed:imgFile];
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:rect];
    [imgView setImage:image];
    [tmpeView addSubview:imgView];
    [imgView release];
    return imgView;
}
+(UIImageView*)addUIImageViewOfPoint:(UIView*)tmpeView imageFile:(NSString*)imgFile point:(CGPoint)point
{
    UIImage *image = [UIImage imageNamed:imgFile];
    CGRect rect = CGRectMake(point.x, point.y, image.size.width, image.size.height);
    UIImageView *mImgView = [[UIImageView alloc]initWithFrame:rect];
    [mImgView setImage:image];
    [tmpeView addSubview:mImgView];
    
    [mImgView release];
    return mImgView;
}


#pragma mark TextFileld
+(UITextField*)addTextFileld:(float)x originY:(float)y width:(float)width height:(float)height
{
    UITextField *textfield = [[UITextField alloc] initWithFrame:CGRectMake(x,y, width, height)];
    [textfield setBorderStyle:UITextBorderStyleLine];
    textfield.placeholder = @"";
    textfield.text = @"";
    textfield.returnKeyType=UIReturnKeyDone;
    textfield.backgroundColor = [UIColor clearColor];
    return textfield;
}
+(UITextField*)addTextFileld:(CGRect)frame
{
    UITextField *textfield = [[UITextField alloc] initWithFrame:frame];
    [textfield setBorderStyle:UITextBorderStyleLine];
    textfield.placeholder = @"";
    textfield.text = @"";
    textfield.returnKeyType=UIReturnKeyDone;
    textfield.backgroundColor = [UIColor clearColor];
    return textfield;
    
}
+(UITextField*)addTextFileldOfScroll:(UIView*)tempView rect:(CGRect)frame string:(NSString*)string placeholder:(NSString*)placeholderStr tag:(int)tag
{
    UIFont *font = [UIFont systemFontOfSize:14];
    CGSize size = [string sizeWithFont:font];
    UIScrollView *scroller = [[UIScrollView alloc]initWithFrame:frame];
    [scroller setBackgroundColor:[UIColor clearColor]];
    [scroller setContentSize:CGSizeMake(size.width,0)];
    [tempView addSubview:scroller];
    [scroller release];

    if (size.width  == 0) {
        size.width = frame.size.width;
    }

    UITextField *textfield = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    [textfield setBorderStyle:UITextBorderStyleNone];
    textfield.tag = tag;
    textfield.font = font;
    [textfield setEnabled:NO];
    textfield.placeholder = placeholderStr;
    textfield.text = string;
    textfield.returnKeyType=UIReturnKeyDone;
    textfield.backgroundColor = [UIColor clearColor];
    [scroller addSubview:textfield];
    [textfield release];
    
    return textfield;
}
#pragma mark UILabel
+(UILabel*)addLabel:(CGRect)rect
{
    UILabel *label = [[UILabel alloc]initWithFrame:rect];
    label.font = [UIFont systemFontOfSize:(int)rect.size.height/2];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor blackColor]];
    return label;
}
+(UILabel*)addLabelOfView:(UIView*)tmpeView  point:(CGPoint)point string:(NSString*)text textSize:(int)TextSize textAlignment:(UITextAlignment)mTextAlignment tag:(int)tag
{
    UIFont *font = [UIFont systemFontOfSize:TextSize];
    CGSize size = [text sizeWithFont:font];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(point.x, point.y, size.width, size.height)];
    label.font = font;
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor blackColor]];
    label.text = text;
    label.tag = tag;
    [tmpeView addSubview:label];
    label.textAlignment = mTextAlignment;
    [label release];
    return label;
}
+(UILabel*)addLabelOfViewRect:(UIView*)tmpeView  rect:(CGRect)rect string:(NSString*)text textSize:(int)TextSize textAlignment:(UITextAlignment)mTextAlignment
{
    UIFont *font = [UIFont systemFontOfSize:TextSize];
    UILabel *label = [[UILabel alloc]initWithFrame:rect];
    label.font = font;
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor blackColor]];
    label.text = text;
    [tmpeView addSubview:label];
    label.textAlignment = mTextAlignment;
    [label release];
    return label;
}
+(UILabel*)addLabelOfScroll:(UIView*)tmpeView  rect:(CGRect)rect string:(NSString*)text textSize:(int)TextSize textAlignment:(UITextAlignment)mTextAlignment
{
    UIFont *font = [UIFont systemFontOfSize:TextSize];
    CGSize size = [text sizeWithFont:font];
    
    UIScrollView *scroller = [[UIScrollView alloc]initWithFrame:rect];
    [scroller setBackgroundColor:[UIColor clearColor]];
    [scroller setContentSize:CGSizeMake(size.width,0)];
    [tmpeView addSubview:scroller];
    [scroller release];
    
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, size.width, rect.size.height)];
    label.font = font;
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor blackColor]];
    label.text = text;
    [scroller addSubview:label];
    label.textAlignment = mTextAlignment;
    [label release];
    return label;
}
#pragma mark -------- UITextView
+(UITextView*)addUITextView:(UIView*)tempView nnstring:(NSString*)string rect:(CGRect)rect
{
    UITextView *textView = [[UITextView alloc]initWithFrame:rect];
    [textView setBackgroundColor:[UIColor clearColor]];
    [textView setEditable:NO];
    [textView setScrollEnabled:YES];
    textView.text = string;
    [tempView addSubview:textView];
    [textView release];
    return textView;
}
#pragma mark -------- UIWebView
+(UIWebView*)addUIWebView:(UIView*)tempview string:(NSString*)string rect:(CGRect)rect
{
    UIWebView *webview = [[UIWebView alloc]initWithFrame:rect];
    [webview loadHTMLString:string baseURL:nil];
    [tempview addSubview:webview];
    [webview release];
    return webview;
}
#pragma mark --------   NSString
+(NSInteger)FormatDateOfeYear:(NSString*)date
{
    return [[date substringToIndex:4] integerValue];
}
+(NSInteger)FormatDateOfeMonths:(NSString*)date
{
    return [[date substringWithRange:NSMakeRange(5, 2)] integerValue];
}
+(NSInteger)FormatDateOfeDay:(NSString*)date
{
    return [[date substringWithRange:NSMakeRange(8, 2)] integerValue];
}
+(NSString*)FormatUrl:(NSString*)string
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    documentDirectory =  [documentDirectory stringByAppendingPathComponent:@"WEBINFO"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:documentDirectory]) {
        NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile:documentDirectory];
        NSString *urlstring =    [NSString stringWithFormat:@"http://%@:%@/dmbk/%@",[dic objectForKey:WEBURLSTRKEY],[dic objectForKey:WEBPOSTSTRKEY],string];
        NSLog(@"urlstring :%@",urlstring);
        return urlstring;
    }
    
    return nil;
    


}
+(NSString*)GetDocumentByString:(NSString*)string
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    documentDirectory = [documentDirectory stringByAppendingPathComponent:string];
    return documentDirectory;
}

+(NSString*)FormatWeek:(int)Weekid
{
    switch (Weekid) {
        case 1:
            return @"星期一";
            break;
        case 2:
            return @"星期二";
            break;
        case 3:
            return @"星期三";
            break;
        case 4:
            return @"星期四";
            break;
        case 5:
            return @"星期五";
            break;
        case 6:
            return @"星期六";
            break;
        case 7:
            return @"星期七";
            break;
        default:
            break;
    }
    return nil;
}
+(NSString*)FormatSection:(int)Sectionid
{
    switch (Sectionid) {
        case 1:
            return @"第一节";
            break;
        case 2:
            return @"第二节";
            break;
        case 3:
            return @"第三节";
            break;
        case 4:
            return @"第四节";
            break;
        case 5:
            return @"第五节";
            break;
        case 6:
            return @"第六节";
            break;
        case 7:
            return @"第七节";
            break;
        case 8:
            return @"第八节";
            break;
        case 9:
            return @"第九节";
            break;
        case 10:
            return @"第十节";
            break;
        case 11:
            return @"第十一节";
            break;
        case 12:
            return @"第十二节";
            break;
        case 13:
            return @"第十三节";
            break;
        case 14:
            return @"第十四节";
            break;
        case 15:
            return @"第十五节";
            break;
        case 16:
            return @"第十六节";
            break;
        case 17:
            return @"第十七节";
            break;

        default:
            break;
    }
    return nil;
}
@end
