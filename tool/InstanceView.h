//
//  InstanceView.h
//  DMPK
//
//  Created by jiaodian on 12-10-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstanceView : NSObject

#pragma mark -------------- UIButton
+(UIButton *)addUIButtonByImageAndAutoSize:(UIView *)fview image:(NSString*)imgFile  point:(CGPoint)point;
+(UIButton *)addUIButtonByImage:(UIView *)fview image:(NSString*)imgFile  rect:(CGRect)rect;


#pragma mark -------------- UIImageview
+(UIImageView*)addUIImageView:(UIView*)tmpeView imageFile:(NSString*)imgFile rect:(CGRect)rect;
+(UIImageView*)addUIImageViewOfPoint:(UIView*)tmpeView imageFile:(NSString*)imgFile point:(CGPoint)point;



#pragma mark --------   TextFileld
+(UITextField*)addTextFileld:(float)x originY:(float)y width:(float)width height:(float)height;
+(UITextField*)addTextFileld:(CGRect)frame;
+(UITextField*)addTextFileldOfScroll:(UIView*)tempView rect:(CGRect)frame string:(NSString*)string placeholder:(NSString*)placeholderStr tag:(int)tag;
#pragma mark --------   UILabel
+(UILabel*)addLabel:(CGRect)rect;
+(UILabel*)addLabelOfView:(UIView*)tmpeView  point:(CGPoint)point string:(NSString*)text textSize:(int)TextSize textAlignment:(UITextAlignment)mTextAlignment tag:(int)tag;
+(UILabel*)addLabelOfViewRect:(UIView*)tmpeView  rect:(CGRect)rect string:(NSString*)text textSize:(int)TextSize textAlignment:(UITextAlignment)mTextAlignment;
+(UILabel*)addLabelOfScroll:(UIView*)tmpeView  rect:(CGRect)rect string:(NSString*)text textSize:(int)TextSize textAlignment:(UITextAlignment)mTextAlignment;
#pragma mark --------   NSString

+(NSInteger)FormatDateOfeYear:(NSString*)date;
+(NSInteger)FormatDateOfeMonths:(NSString*)date;
+(NSInteger)FormatDateOfeDay:(NSString*)date;

#pragma mark -------- UITextView
+(UITextView*)addUITextView:(UIView*)tempView nnstring:(NSString*)string rect:(CGRect)rect;
#pragma mark -------- UIWebView
+(UIWebView*)addUIWebView:(UIView*)tempview string:(NSString*)string rect:(CGRect)rect;


+(NSString*)FormatUrl:(NSString*)string;
+(NSString*)FormatWeek:(int)Weekid;
+(NSString*)FormatSection:(int)Sectionid;
+(NSString*)GetDocumentByString:(NSString*)string;
@end
