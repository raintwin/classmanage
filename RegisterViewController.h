//
//  RegisterViewController.h
//  DMPK
//
//  Created by jiaodian on 12-11-14.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController<UITextFieldDelegate,HttpFormatRequestDeleagte,UIAlertViewDelegate>

@property(nonatomic,retain)NSMutableDictionary *mRegisterDict;
@property(nonatomic,retain)HttpFormatRequest *mHttpRequest;

@end
