//
//  SyllabusScrollView.m
//  DMPK
//
//  Created by jiaodian on 12-10-30.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "SyllabusScrollView.h"
#import "InstanceView.h"
#import "InstanceObject.h"

@implementation SyllabusScrollView
@synthesize maxsectionId;
@synthesize delegeta;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
//        [InstanceView addUIImageViewOfPoint:self imageFile:@"座位表大背景.png" point:CGPointMake(0,0)];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
#define PointY 0
#define PointX 0
-(void)initData:(NSMutableArray*)mDataArray
{
    UIImage *image = [UIImage imageNamed:@"课程表单个背景.png"];

    for (int i = 0; i<maxsectionId; i++) {
        [InstanceView addUIImageViewOfPoint:self imageFile:@"座位表单行背景.png" point:CGPointMake(PointX,PointY+i*38)];

        if (i%2==0) {
            [InstanceView addUIImageViewOfPoint:self imageFile:@"课程表单个背景.png" point:CGPointMake(PointX,PointY+i*38)];
        }
        
        UILabel *label = [InstanceView addLabel:CGRectMake(PointX, PointY+1+i*38, 79, 37)];
        label.text = [NSString stringWithFormat:@"第%d节",i+1];
        label.textAlignment = UITextAlignmentCenter;
        [self addSubview:label];
        [label release];
    }

    for (int i = 0; i<[mDataArray count]; i++) {
        InstanceObject *mInstanceObject = [mDataArray objectAtIndex:i];
        
        NSString *title = [NSString stringWithFormat:@"%@\n%@",mInstanceObject.gradeName,mInstanceObject.subjectName];

        CGSize mSize=image.size;
        UIButton *mButton = [InstanceView addUIButtonByImage:self image:@"nil" 
                                                        rect:CGRectMake(PointX+(mSize.width+1)*mInstanceObject.weekId,PointY+(mInstanceObject.sectionId-1)*38,mSize.width,mSize.height)];
        [mButton setTitle:title forState:0];
        [mButton setShowsTouchWhenHighlighted:YES];
        mButton.titleLabel.font = [UIFont systemFontOfSize:13];
        mButton.titleLabel.textAlignment = UITextAlignmentCenter;
        mButton.titleLabel.lineBreakMode = UILineBreakModeWordWrap;
        mButton.titleLabel.numberOfLines = 2;
        mButton.tag = i;
        [mButton addTarget:self action:@selector(GradeSubject:) forControlEvents:64];
        if (mInstanceObject.sectionId %2 !=0) {
            [mButton setBackgroundImage:image forState:0];
            [mButton setTitleColor:[UIColor whiteColor] forState:0];
        }
        else {
            [mButton setBackgroundImage:nil forState:0];
            [mButton setBackgroundColor:[UIColor clearColor]];
            [mButton setTitleColor:[UIColor blackColor] forState:0];
        }
        
        if ([mInstanceObject.isDefault intValue] == 0) {
            [mButton setBackgroundImage:[UIImage imageNamed:@"课程表单个背景2.png"] forState:0];
            
        }
    }
}

-(void)GradeSubject:sender
{
    UIButton *mButton = sender;
    [delegeta ResponseScroller:mButton.tag];
}
-(void)dealloc
{
    [super dealloc];
}
@end
