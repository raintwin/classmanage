//
//  StudentEvaluateMainController.h
//  DMPK
//
//  Created by jiaodian on 12-11-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstanceView.h"
#import "StudentEvaluateDetailController.h"
@interface StudentEvaluateMainController : UIViewController<HttpFormatRequestDeleagte,UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,retain)NSMutableDictionary *mDataDict;
@property(nonatomic,retain)NSMutableArray *mStudents;
@property(nonatomic,retain)HttpFormatRequest *mHttpRequest;

@end
