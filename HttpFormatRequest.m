//
//  HttpFormatRequest.m
//  texthttp
//
//  Created by jiaodian on 12-10-26.
//
//

#import "HttpFormatRequest.h"
#import "ASIFormDataRequest.h"
#import "JSON.h"
#import "SVProgressHUD.h"
@implementation HttpFormatRequest
@synthesize Delegate;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
-(void)handleSingleTapFrom
{
    NSLog(@"touch ");
}
-(void)CustomAddGestureRecognizer
{
    [[UIApplication sharedApplication].keyWindow removeGestureRecognizer:GlobalsingleRecognizer];
    GlobalsingleRecognizer = nil;
    GlobalsingleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapFrom)];
    GlobalsingleRecognizer.numberOfTapsRequired = 1; // 单击
    GlobalsingleRecognizer.numberOfTouchesRequired = 1; //手指数
    [[UIApplication sharedApplication].keyWindow addGestureRecognizer:GlobalsingleRecognizer];
    [GlobalsingleRecognizer release];
}
-(void)CustomDeleteGestureRecognizer
{
    [[UIApplication sharedApplication].keyWindow removeGestureRecognizer:GlobalsingleRecognizer];
    GlobalsingleRecognizer = nil;

}

-(void)CustomFormDataRequestDict:(NSString*)JSONString tag:(int)tag url:(NSString*)UrlString
{
    
    NSLog(@"URL:%@",UrlString);
    NSLog(@"Foucion JSONString-- :%@",JSONString);
    
    [self CustomAddGestureRecognizer];

    
    NSURL *url = [NSURL URLWithString:UrlString];
    
    [SVProgressHUD show];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    request.tag = tag;
    [request setDelegate:self];
    [request setTimeOutSeconds:20];
    [request setPostValue:JSONString forKey:@"requestContent"];
    [request startAsynchronous];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [SVProgressHUD dismiss];
    [self CustomDeleteGestureRecognizer];

    NSData *responeseData = [request responseData];
    NSString *responseStr = [[NSString alloc] initWithData:responeseData encoding:NSUTF8StringEncoding];
    
    
    responseStr =   [responseStr stringByReplacingOccurrencesOfString: @"\r" withString:@""];
    responseStr =   [responseStr stringByReplacingOccurrencesOfString: @"\n" withString:@""];
    
    NSLog(@"---- responseStr %@",responseStr);
    NSMutableDictionary *mDict = [responseStr JSONValue];
    [responseStr release];
    
    int ResultCode = [[[mDict objectForKey:@"ResultStatus"] objectForKey:@"ResultCode"] intValue];
    NSString *ResultCodeString = [[mDict objectForKey:@"ResultStatus"] objectForKey:@"ResultDescription"];


    if (ResultCode == 0) {
        NSLog(@"发送正确，完成发送");
    }
    else if (ResultCode == 1000 )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"用户名或密码错误" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else if (ResultCode == 1 || ResultCode ==2 || ResultCode == 3 || ResultCode == 4  || ResultCode == 5 || ResultCode == 100)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:ResultCodeString delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        
        NSLog(@"ResultCodeString = %@   ResultCode = %d",ResultCodeString,ResultCode);
//        [Delegate HttpRequestFinish:ResultCodeString tag:BACKRESULTCODE+ResultCode];

        return;
    }
    NSString *ResultContent = [[mDict objectForKey:@"ResultContent"] JSONRepresentation];

    if (ResultCode == 0 ) {
        [Delegate HttpRequestFinish:ResultContent tag:request.tag];
    }
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    [self CustomDeleteGestureRecognizer];
    NSData *responeseData = [request responseData];
    NSString *responseStr = [[NSString alloc] initWithData:responeseData encoding:NSUTF8StringEncoding];
    
    
    responseStr =   [responseStr stringByReplacingOccurrencesOfString: @"\r" withString:@""];
    responseStr =   [responseStr stringByReplacingOccurrencesOfString: @"\n" withString:@""];
    
    NSLog(@"---- responseStr %@",responseStr);
    
    [responseStr release];
    [Delegate HttpRequestFailed:responseStr tag:request.tag];
    [SVProgressHUD dismiss];
    

    
}

@end
