//
//  StudentEvaluateDetailController.h
//  DMPK
//
//  Created by jiaodian on 12-11-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstanceView.h"
#import "InstanceObject.h"
@interface StudentEvaluateDetailController : UIViewController<HttpFormatRequestDeleagte,UITableViewDelegate,UITableViewDataSource>
{
    int mItemId;
}
@property(nonatomic,assign)int evaluateMainId;
@property(nonatomic,retain)NSMutableArray *mEvaluateDetails;
@property(nonatomic,retain)NSMutableDictionary *mInfoDict;

@property(nonatomic,retain)HttpFormatRequest *mHttpRequest;

@end

