//
//  SearchClassroomViewController.h
//  DMPK
//
//  Created by jiaodian on 12-11-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchClassroomViewControllerDelegate <NSObject>

-(void)GetChooseClassroomPlan:(int)ClassId;

@end

@interface SearchClassroomViewController : UIViewController<HttpFormatRequestDeleagte,UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate>
{
    id<SearchClassroomViewControllerDelegate>delegate;
}
@property(nonatomic,retain)NSMutableDictionary *mDataDict;
@property(nonatomic,retain)NSMutableArray *mPlans;
@property(nonatomic,assign)id<SearchClassroomViewControllerDelegate>delegate;
@property(nonatomic,assign)BOOL IsSyllabus;
@property(nonatomic,retain)HttpFormatRequest *mHttpRequest;

@end

