//
//  SearchClassroomViewController.m
//  DMPK
//
//  Created by jiaodian on 12-11-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "SearchClassroomViewController.h"
#import "InstanceView.h"
#import "InstanceObject.h"
#import "MainViewCtrl.h"
#define TableviewHeight 38


@interface SearchClassroomViewController ()

@end

@implementation SearchClassroomViewController
@synthesize mDataDict;
@synthesize mPlans;
@synthesize delegate;
@synthesize IsSyllabus;
@synthesize mHttpRequest;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        IsSyllabus = NO;
    }
    return self;
}
-(void)SetBackNav
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)LoadDataHttp
{
    {
        HttpFormatRequest *http = [[HttpFormatRequest alloc]init];
        http.Delegate = self;
        self.mHttpRequest = http;
        [http release];
        [self.view addSubview:mHttpRequest];
        
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:[mDataDict objectForKey:@"termId"] forKey:@"termId"];
        [mDict setObject:[mDataDict objectForKey:@"teacherId"] forKey:@"teacherId"];
        [mDict setObject:[mDataDict objectForKey:@"gradeId"] forKey:@"gradeId"];
        [mDict setObject:[mDataDict objectForKey:@"date"] forKey:@"date"];        
        NSString *string = [mDict JSONRepresentation];
        [mDict release];
        [mHttpRequest CustomFormDataRequestDict:string tag:0 url:[InstanceView FormatUrl:@"ClientSearchClassroom"]];
    }

}
-(void)SelectDate
{
    NSString *title = UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation) ? @"\n\n\n\n\n\n\n\n\n\n\n" : @"\n\n\n\n\n\n\n\n\n\n\n\n\n\n" ;

    UIActionSheet *shareSheet = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"确定",nil];
    
    shareSheet.delegate = self;
    shareSheet.tag = 101010;
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 270,180)];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.tag = 1101;
    [shareSheet addSubview:datePicker];
    [datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];    
    [shareSheet showInView:self.view];
    [datePicker release];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *now = [dateFormatter stringFromDate:datePicker.date];
    
    
    [dateFormatter release];
    [mDataDict setObject:now forKey:@"date"];

}
-(void)dateChanged:(id)sender{    
    
    UIDatePicker *control = (UIDatePicker*)sender;    
    NSDate* _date = control.date;  
    
    //    NSDateComponents *comps = [[NSDateComponents alloc]init];
    
    /*添加你自己响应代码*/    
    // ---  日期
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *now = [dateFormatter stringFromDate:_date];
    
    
    [dateFormatter release];
    [mDataDict setObject:now forKey:@"date"];
    NSLog(@"日期:%@",now);
    
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex;
{

    ((UILabel*)[self.view viewWithTag:10111]).text = [mDataDict objectForKey:@"date"];
    [self LoadDataHttp];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [InstanceView addUIImageViewOfPoint:self.view imageFile:@"查看历史课堂_背景.png" point:CGPointMake(0,0)];

    {
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"返回.png" point:CGPointMake(29, 10)];
        [mButton addTarget:self action:@selector(SetBackNav) forControlEvents:64];
    }
    {
        NSMutableArray *arry = [[NSMutableArray alloc]init];
        self.mPlans = arry;
        [arry release];
    }
    {
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"上课日期按钮.png" point:CGPointMake(103, 115)];
        [mButton addTarget:self action:@selector(SelectDate) forControlEvents:64];
    }

    {
        UILabel *label = [InstanceView addLabelOfViewRect:self.view rect:CGRectMake(190, 115, 400, 29) string:[mDataDict objectForKey:@"date"] textSize:15 textAlignment:UITextAlignmentLeft];
        label.tag = 10111;
    }
    [self LoadDataHttp];
    
    UITableView *mTableView = [[UITableView alloc]initWithFrame:CGRectMake(103, 181, 824, 495) style:UITableViewStylePlain];
    mTableView.delegate = self;
    mTableView.dataSource  = self;
    mTableView.tag = 100111;
    [self.view addSubview:mTableView];
    [mTableView release];
    NSLog(@"mDateDict --:%@",[mDataDict JSONRepresentation]);
}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag1
{
    [mPlans removeAllObjects];
    NSArray *plans = [[[ResultContent JSONValue] valueForKey:@"classrooms"]retain];
    for (int i = 0; i<[plans count]; i++) {
        
        PlanObject *mPlanObject = [[PlanObject alloc]init];
        mPlanObject.planId = [[NSString stringWithFormat:@"%@",[[plans objectAtIndex:i] valueForKey:@"id"]] intValue];
        mPlanObject.name = [NSString stringWithFormat:@"%@",[[plans objectAtIndex:i] valueForKey:@"name"]];
        mPlanObject.subjectName = [NSString stringWithFormat:@"%@",[[plans objectAtIndex:i] valueForKey:@"subjectName"]];
        mPlanObject.teacherName = [NSString stringWithFormat:@"%@",[[plans objectAtIndex:i] valueForKey:@"teacherName"]];
        [mPlans addObject:mPlanObject];
        [mPlanObject release];
    }
    [plans release];
    [((UITableView*)[self.view viewWithTag:100111]) reloadData];
    if ([mPlans count] <13) {
        [((UITableView*)[self.view viewWithTag:100111]) setFrame:CGRectMake(103, 181, 824, TableviewHeight * [mPlans count])];
    }
}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *cellName = @"cellstr";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if (cell== nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
        cell.textLabel.text = @"";
        int textSize = 14;
        [InstanceView addUIImageViewOfPoint:cell.contentView imageFile:@"查看历史课堂_行.png" point:CGPointMake(0,0)];
        {
            UILabel *label = [InstanceView addLabelOfViewRect:cell.contentView rect:CGRectMake(0, 0, 35, TableviewHeight) string:@"" textSize:textSize textAlignment:UITextAlignmentCenter];
            label.tag = 20011;
        }
        {
            UILabel *label = [InstanceView addLabelOfViewRect:cell.contentView rect:CGRectMake(35, 0, 550, TableviewHeight) string:@"" textSize:textSize textAlignment:UITextAlignmentCenter];
            label.tag = 20022;
        }
        {
            UILabel *label = [InstanceView addLabelOfViewRect:cell.contentView rect:CGRectMake(585, 0, 120, TableviewHeight) string:@"" textSize:textSize textAlignment:UITextAlignmentCenter];
            label.tag = 20033;
        }
        {
            UILabel *label = [InstanceView addLabelOfViewRect:cell.contentView rect:CGRectMake(705, 0, 120, TableviewHeight) string:@"" textSize:textSize textAlignment:UITextAlignmentCenter];
            label.tag = 20044;
        }
    }
        PlanObject *mPlanObject = [mPlans objectAtIndex:[indexPath row]];
        ((UILabel*)[cell.contentView viewWithTag:20011]).text = [NSString stringWithFormat:@"%d",[indexPath row]+1];
        ((UILabel*)[cell.contentView viewWithTag:20022]).text = mPlanObject.name;
        ((UILabel*)[cell.contentView viewWithTag:20033]).text = mPlanObject.teacherName;
        ((UILabel*)[cell.contentView viewWithTag:20044]).text = mPlanObject.subjectName;

    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return [mPlans count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TableviewHeight;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PlanObject *mPlanObject = [mPlans objectAtIndex:[indexPath row]];
    
    if (IsSyllabus) {
        [mDataDict setObject:[NSNumber numberWithInt:mPlanObject.planId] forKey:@"Historyid"];
        
        NSLog(@"mDataDict :%@",[mDataDict JSONRepresentation]);
        MainViewCtrl *mMainViewCtrl = [[MainViewCtrl alloc]initWithNibName:@"MainViewCtrl" bundle:nil];
        mMainViewCtrl.mDataDict = mDataDict;
        mMainViewCtrl.IsHistory = YES;
        [self.navigationController pushViewController:mMainViewCtrl animated:YES];
        [mMainViewCtrl release];

    }
    else {
        PlanObject *mPlanObject = [mPlans objectAtIndex:[indexPath row]];
        [delegate GetChooseClassroomPlan:mPlanObject.planId];
        [self.navigationController popViewControllerAnimated:YES];

    }
}
-(void)dealloc
{
    [mDataDict release];
    [mPlans release];
    [mHttpRequest release];
//    [delegate release];
    [super dealloc];
}
- (void)viewDidUnload
{
    self.mDataDict = nil;
    self.mPlans = nil;
    self.mHttpRequest = nil;
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;

//    self.delegate = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
