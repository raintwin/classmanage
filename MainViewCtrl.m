//
//  MainViewCtrl.m
//  DMPK
//
//  Created by jiaodian on 12-10-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "MainViewCtrl.h"
#import "InstanceView.h"
#import "planViewController.h"
#import "AppDelegate.h"

@interface MainViewCtrl ()
{
    int clientPlanFlag;
}
@property (nonatomic,retain) NSMutableArray *planSeniors;
@property (nonatomic,retain) UINavigationController *navPlan;
@property (nonatomic,retain) UIButton *currentBookButton;
@end

@implementation MainViewCtrl

@synthesize mInstanceObject;
@synthesize mDataDict;
@synthesize mStudents;
@synthesize mPlans;
@synthesize mButtons;
@synthesize mInfoDict;
@synthesize mAttendances;
@synthesize mFetchEbooks;
@synthesize mButtonTitles;
@synthesize IsHistory;
@synthesize mHttpRequest;

@synthesize planSeniors;
@synthesize navPlan;
@synthesize currentBookButton;

#define buttonlineheight 43
#define TableviewHeight 41

#define ClientClassroomHTTPTAG 200111
#define ClientSearchPlanHTTPTAG 200222
#define ClientChoosePlanHTTPTAG 200333
#define ClientSaveClassroomHTTPTAG 200444
#define AttendanceHTTPTAG 200555
#define FetchEbookHTTPTAG 200666

#define ClientSeniorHTTPTAG 300111
#define ClientSubjectHTTPTAG 300222
#define ClientVersionHTTPTAG 300333

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        mClassroomID = -1;
        IsHistory = NO;
        mButtonStatusID = 0;
        clientPlanFlag = -1;
        catalogId = -1;
    }
    return self;
}
-(void)initViewData
{
    {
        NSMutableArray *array = [[NSMutableArray alloc]init];
        self.mStudents = array;
        [array release];
    }
    {
        NSMutableArray *array = [[NSMutableArray alloc]init];
        self.mPlans = array;
        [array release];
    }
    {
        NSMutableArray *array = [[NSMutableArray alloc]init];
        self.mAttendances = array;
        [array release];
    }
    {
        NSMutableArray *array = [[NSMutableArray alloc]init];
        self.mButtons = array;
        [array release];
    }
    {
        NSMutableArray *array = [[NSMutableArray alloc]init];
        self.mFetchEbooks = array;
        [array release];
    }
    {
        NSMutableArray *array = [[NSMutableArray alloc]init];
        self.mButtonTitles = array;
        [array release];
    }
    

    {
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        self.mInfoDict = mDict;
        [mDict release];
    }
    
    {
        HttpFormatRequest *http = [[HttpFormatRequest alloc]init];
        http.Delegate = self;
        self.mHttpRequest = http;
        [http release];

        [self.view addSubview:mHttpRequest];
        
    }
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"主界面背景.png"]]];
    
//    self.planSeniors = [[NSMutableArray alloc]init];
}
-(void)initView
{
    [InstanceView addUIImageViewOfPoint:self.view imageFile:@"教案背景.png" point:CGPointMake(360,117)];

    {
        UIImage *image = [UIImage imageNamed:@"教案背景.png"];
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(360, 117, image.size.width,image.size.height)];
        view.tag = 1000011;
        [self.view addSubview:view];
        [view release];
    }
    {
        UIScrollView *mScroller = [[UIScrollView alloc]initWithFrame:CGRectMake(29, 95, 300, 5*63)];
        mScroller.tag = 2001;
        [mScroller setBackgroundColor:[UIColor clearColor]];
        [mScroller setPagingEnabled:YES];
        [self.view addSubview:mScroller];
        [mScroller release];
    }
    
    {
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"迟到.png" point:CGPointMake(40, 425)];
        [mButton addTarget:self action:@selector(StudentLate) forControlEvents:64];
        mButton.tag = 8001;
    }
    {
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"未到.png" point:CGPointMake(110, 425)];
        [mButton addTarget:self action:@selector(StudentNonArrival) forControlEvents:64];
        mButton.tag = 8002;

    }
    {
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"请假.png" point:CGPointMake(180, 425)];
        [mButton addTarget:self action:@selector(StudentLeave) forControlEvents:64];
        mButton.tag = 8003;

    }
    {
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"随机点名.png" point:CGPointMake(29, 475)];
        [mButton addTarget:self action:@selector(StudentDOT) forControlEvents:64];
    }
    {
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"电子书按钮.png" point:CGPointMake(29, 475+43)];
        [mButton addTarget:self action:@selector(StudentEBOOK) forControlEvents:64];
    }
    {
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"教师评价.png" point:CGPointMake(29, 475+43*2)];
        [mButton addTarget:self action:@selector(TeaherAssess) forControlEvents:64];
    }
    {
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"学生评价.png" point:CGPointMake(29, 475+43*3)];
        [mButton addTarget:self action:@selector(StudentAssess) forControlEvents:64];
    }
    {
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"历史课堂.png" point:CGPointMake(29, 475+43*4)];
        [mButton addTarget:self action:@selector(HistoryClass) forControlEvents:64];
    }
    {
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"返回.png" point:CGPointMake(29, 10)];
        [mButton addTarget:self action:@selector(SetBackNav) forControlEvents:64];
    }
    {
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"全屏.png" point:CGPointMake(290, 60)];
        [mButton addTarget:self action:@selector(SetStudentScreen) forControlEvents:64];
    }
    
    {
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"全屏.png" point:CGPointMake(960, 60)];
        [mButton addTarget:self action:@selector(SetPlansScreen) forControlEvents:64];
    }
    {
        self.currentBookButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"textBook.png" point:CGPointMake(600, 655)];
        [self.currentBookButton addTarget:self action:@selector(openCurrentBook) forControlEvents:64];
        self.currentBookButton.hidden = YES;
    }

}
#pragma mark 返回
-(void)SetBackNav
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)RelodateOfStudents
{
    for (InstanceStudent *mInstanceStudent in mStudents) {
        mInstanceStudent.IsMark = NO;
    }
    for (UIButton *mBtn in mButtonTitles) {
        [self setButtonTitleColor:mBtn status:mButtonStatusID];
    }
    
    [mButtonTitles removeAllObjects];
}
#pragma mark 设置学生位置全屏
-(void)SetStudentScreen
{
    UIScrollView *mSrcoll = (UIScrollView*)[self.view viewWithTag:2001];
    [mSrcoll setFrame:CGRectMake(0, 0, 1024, 748)];
    [mSrcoll setBackgroundColor:[UIColor grayColor]];
    int mRow = 0;
    int mCol = 0;
    for (int i = 0; i<[mStudents count]; i++) {
        if (i%5==0) {
            UIImageView *mImageView =(UIImageView*)[mSrcoll viewWithTag:1212100+i];
            [mImageView setHidden:YES];
        }

    InstanceStudent *mInstanceStudent = [mStudents objectAtIndex:i];
    UIButton *mBTN = (UIButton*)[mSrcoll viewWithTag:i];
    CGPoint point = CGPointMake(10+80*(mInstanceStudent.colsId-1),10+80*(mInstanceStudent.rowsId-1));
    [mBTN setFrame:CGRectMake(point.x, point.y, 44, 38)];    
    [mBTN setTitleEdgeInsets:UIEdgeInsetsMake(5, 0, -70, 0)];
    mBTN.titleLabel.font = [UIFont boldSystemFontOfSize:13];
    mBTN.titleLabel.textAlignment = UITextAlignmentCenter;
        [self setButtonTitleColor:mBTN status:mInstanceStudent.status];
        if (i == 0) {
            mRow = mInstanceStudent.rowsId;
            mCol = mInstanceStudent.colsId;
        }
        if (mRow< mInstanceStudent.rowsId) {
            mRow = mInstanceStudent.rowsId;
        }
        if (mCol< mInstanceStudent.colsId) {
            mCol = mInstanceStudent.colsId;
        }
    }
    [mSrcoll setContentSize:CGSizeMake(8+80*mCol, 80*mRow)];
    [self.view bringSubviewToFront:mSrcoll];
    [self.view bringSubviewToFront:(UIButton*)[self.view viewWithTag:8001]]; [((UIButton*)[self.view viewWithTag:8001]) setFrame:CGRectMake(900, 400, 54, 29)];
    [self.view bringSubviewToFront:(UIButton*)[self.view viewWithTag:8002]]; [((UIButton*)[self.view viewWithTag:8002]) setFrame:CGRectMake(900, 500, 54, 29)];
    [self.view bringSubviewToFront:(UIButton*)[self.view viewWithTag:8003]]; [((UIButton*)[self.view viewWithTag:8003]) setFrame:CGRectMake(900, 600, 54, 29)];
    
    UIButton *mButtonBack = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"分屏2.png" point:CGPointMake(900, 5)];    
    [mButtonBack addTarget:self action:@selector(SetStudentScreenMin:) forControlEvents:64];
}
-(void)SetStudentScreenMin:sender
{
    UIButton *mTempButton = (UIButton *)sender;
    [mTempButton removeFromSuperview];
    
    UIScrollView *mSrcoll = (UIScrollView*)[self.view viewWithTag:2001];
    [mSrcoll setFrame:CGRectMake(29, 95, 300, 5*63)];
    [mSrcoll setBackgroundColor:[UIColor clearColor]];
    int mRow = 0;
    int mCol = 0;
    for (int i = 0; i<[mStudents count]; i++) {
        if (i%5==0) {
            UIImageView *mImageView =(UIImageView*)[mSrcoll viewWithTag:1212100+i];
            [mImageView setHidden:YES];
        }
        
        InstanceStudent *mInstanceStudent = [mStudents objectAtIndex:i];
        UIButton *mBTN = (UIButton*)[mSrcoll viewWithTag:i];
        CGPoint point = CGPointMake(8+48*(mInstanceStudent.colsId-1),63*(mInstanceStudent.rowsId-1));
        [mBTN setFrame:CGRectMake(point.x, point.y, 44, 38)];
        [mBTN setTitleEdgeInsets:UIEdgeInsetsMake(5, 0, -45, 0)];
        mBTN.titleLabel.font = [UIFont systemFontOfSize:12];
        [self setButtonTitleColor:mBTN status:mInstanceStudent.status];
        if (i == 0) {
            mRow = mInstanceStudent.rowsId;
            mCol = mInstanceStudent.colsId;
        }
        if (mRow< mInstanceStudent.rowsId) {
            mRow = mInstanceStudent.rowsId;
        }
        if (mCol< mInstanceStudent.colsId) {
            mCol = mInstanceStudent.colsId;
        }
    }
    [((UIScrollView*)[self.view viewWithTag:2001]) setContentSize:CGSizeMake(8+48*(mCol),mRow* 63)];
    [self.view bringSubviewToFront:mSrcoll];

    [self.view bringSubviewToFront:(UIButton*)[self.view viewWithTag:8001]]; [((UIButton*)[self.view viewWithTag:8001]) setFrame:CGRectMake(40, 425, 54, 29)];
    [self.view bringSubviewToFront:(UIButton*)[self.view viewWithTag:8002]]; [((UIButton*)[self.view viewWithTag:8002]) setFrame:CGRectMake(110, 425, 54, 29)];
    [self.view bringSubviewToFront:(UIButton*)[self.view viewWithTag:8003]]; [((UIButton*)[self.view viewWithTag:8003]) setFrame:CGRectMake(180, 425, 54, 29)];
    

}

-(void)SendStudentStatus:(int)status
{
    NSMutableArray *mStudentStatus = [[NSMutableArray alloc]init];
    for (InstanceStudent *mInstanceStudent in mStudents) {
        if (mInstanceStudent.IsMark) {
            [mStudentStatus addObject:[NSNumber numberWithInt:mInstanceStudent.studentId]];
        }
    }
    {
        mHttpRequest.Delegate = self;
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:[mDataDict objectForKey:@"id"] forKey:@"id"];
        [mDict setObject:[NSNumber numberWithInt:status] forKey:@"status"];
        [mDict setObject:mStudentStatus forKey:@"students"];
        [mStudentStatus release];
        
        NSString *string = [mDict JSONRepresentation];
        [mDict release];
        [mHttpRequest CustomFormDataRequestDict:string tag:AttendanceHTTPTAG url:[InstanceView FormatUrl:@"ClientAttendance"]];
    }

}
#pragma mark 迟到
-(void)StudentLate
{
    mButtonStatusID = 1;
    [self SendStudentStatus:mButtonStatusID];
    
}
#pragma mark 未到
-(void)StudentNonArrival
{
    mButtonStatusID = 2;
    [self SendStudentStatus:mButtonStatusID];
}
#pragma mark 请假
-(void)StudentLeave
{
    mButtonStatusID = 3;
    [self SendStudentStatus:mButtonStatusID];
}
#pragma mark 随便点名
-(void)StudentDOT
{
    if ([mStudents count] == 0) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"提示" message:@"暂无学生" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertview show];
        [alertview release];
        return;
    }
    UIView *mView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 1024, 748)];
    mView.tag = 40001;
    [mView setBackgroundColor:[UIColor blackColor]];
    [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:mView];
    [mView release];
    
//    InstanceStudent *mInstanceStudent = [[InstanceStudent alloc]init];
    
    InstanceStudent *mInstanceStudent = [mStudents objectAtIndex:rand()%[mStudents count]];
    
    NSString *genderString = @"";
    if ([mInstanceStudent.gender isEqualToString:@"男"]) {
        genderString = @"男同学2.png";
    }
    else {
        genderString = @"女同学2.png";
    }
    UIButton *BTN = [InstanceView addUIButtonByImageAndAutoSize:[UIApplication sharedApplication].keyWindow.rootViewController.view image:genderString point:CGPointMake(0, 0)];

    [BTN setTitleColor:[UIColor redColor] forState:0];
    [BTN setTitle:mInstanceStudent.studentName forState:0];
    [BTN setTitleEdgeInsets:UIEdgeInsetsMake(20, 10, -70, 0)];
    BTN.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    BTN.titleLabel.textAlignment = UITextAlignmentCenter;
    
    [BTN setCenter:CGPointMake(512, 318)];
    [BTN setTitle:mInstanceStudent.studentName forState:0];

    BTN.tag = 40002;
    [BTN addTarget:self action:@selector(CloseView) forControlEvents:64];
}
#pragma mark 电子书
-(void)StudentEBOOK
{
    planViewController *planViewCrtl = [[planViewController alloc]initWithNibName:@"planViewController" bundle:nil];
    planViewCrtl.cleintRequest = @"ClientVersion";
    planViewCrtl.subjectId = mInstanceObject.subjectId;
    planViewCrtl.seniorId = mInstanceObject.seniorId;
    planViewCrtl.titleString = @"请选择版本";
    planViewCrtl.isBook = YES;
    self.navPlan = [[UINavigationController alloc] initWithRootViewController:planViewCrtl];
    [self.navPlan setNavigationBarHidden:YES];
    [self.navPlan.view setFrame:CGRectMake(360, 117, 604, 578)];
    
    [self.view addSubview:self.navPlan.view];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateBookNotification:)
                                                 name:NOTIFICATION_BOOK
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateSendQuesttionUserProfileNotification:)
                                                 name:NOTIFICATION_PLAN
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closeNavPlanNotification:)
                                                 name:NOTIFICATION_CLOSEPLAN
                                               object:nil];

    
    
    return;

}
- (void) closeNavPlanNotification:(NSNotification *) notification
{
    [self.navPlan.view removeFromSuperview];

}
- (void) updateBookNotification:(NSNotification *) notification
{
    
    NSMutableDictionary *dict = (NSMutableDictionary*) [[NSUserDefaults standardUserDefaults] objectForKey:NOTIFICATION_QUESTPLAN];
    NSLog(@"选择教案 = %@",dict);
    
    
//    NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
//    [mDict setObject:[NSNumber numberWithInt:mClassroomID ] forKey:@"id"];
//    [mDict setObject:[dict objectForKey:@"planId"] forKey:@"planId"];
//    
//    NSString *string = [mDict JSONRepresentation];
//    [mDict release];
//    
//    [mHttpRequest CustomFormDataRequestDict:string tag:ClientChoosePlanHTTPTAG url:[InstanceView FormatUrl:@"ClientChoosePlan"]];
    
    catalogId = [[dict objectForKey:@"catalogId"] intValue];
    
    
    plandId = [[dict objectForKey:@"plandId"] intValue];

    [self openCurrentBook];
}
#pragma mark 打开当前电子书

-(void)openCurrentBook
{

    NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];

    

    if (catalogId<0) {
        [mDict setObject:mInfoDict[@"planId"] forKey:@"planId"];
    }
    else
    {
        [mDict setObject:[NSNumber numberWithInt:catalogId] forKey:@"catalogId"];

    }
    NSString *string = [mDict JSONRepresentation];
    
    [mDict release];
    mHttpRequest.Delegate = self;
    [mHttpRequest CustomFormDataRequestDict:string tag:FetchEbookHTTPTAG url:[InstanceView FormatUrl:@"ClientFetchEbook"]];

}
#pragma mark 教师评价
-(void)TeaherAssess
{
    NSLog(@"老师评价 :%@",[mDataDict JSONRepresentation]);
    TeacherSaveEvaluateController *mTeacherSaveEvaluate = [[TeacherSaveEvaluateController alloc]initWithNibName:@"TeacherSaveEvaluateController" bundle:nil];
    mTeacherSaveEvaluate.mDataDict = mDataDict;
    [self.navigationController pushViewController:mTeacherSaveEvaluate animated:YES];
    [mTeacherSaveEvaluate release];
}
#pragma mark 学生评价
-(void)StudentAssess
{
    StudentEvaluateMainController *mStudentEvaluateDetail = [[StudentEvaluateMainController alloc]initWithNibName:@"StudentEvaluateMainController" bundle:nil];
    mStudentEvaluateDetail.mDataDict = mDataDict;
    [self.navigationController pushViewController:mStudentEvaluateDetail animated:YES];
    [mStudentEvaluateDetail release];
}
#pragma mark 历史课堂
-(void)HistoryClass
{
    NSLog(@"mDataDict 22 --- :%@",[mDataDict JSONRepresentation]);
    
    SearchClassroomViewController *mSearchClassroom = [[SearchClassroomViewController alloc]initWithNibName:@"SearchClassroomViewController" bundle:nil];
    mSearchClassroom.mDataDict = mDataDict;
    mSearchClassroom.delegate = self;
    [self.navigationController pushViewController:mSearchClassroom animated:YES];
    [mSearchClassroom release];
}
-(void)GetChooseClassroomPlan:(int)ClassId
{
    NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
    [mDict setObject:[NSNumber numberWithInt:ClassId] forKey:@"id"];
    NSString *string = [mDict JSONRepresentation];
    [mDict release];

    [self performSelector:@selector(ClientChooseClassroom:) withObject:string afterDelay:0.1f];
}
-(void)ClientChooseClassroom:(NSString*)string
{
    mHttpRequest.Delegate = self;
    
    [mHttpRequest CustomFormDataRequestDict:string tag:ClientClassroomHTTPTAG url:[InstanceView FormatUrl:@"ClientChooseClassroom"]];
}

#pragma mark 保存教案
-(void)SavaPlan
{
    mHttpRequest.Delegate = self;
    if ([((UITextField*)[self.view viewWithTag:3330111]).text length]>0) {
        [mInfoDict setObject:((UITextField*)[self.view viewWithTag:3330111]).text forKey:@"reflect"];
    }
    else {
        [mInfoDict setObject:@" " forKey:@"reflect"];
    }
    if ([((UITextField*)[self.view viewWithTag:3330222]).text length]>0) {
        [mInfoDict setObject:((UITextField*)[self.view viewWithTag:3330222]).text forKey:@"extend"];
    }
    else {
        [mInfoDict setObject:@" " forKey:@"extend"];

    }

    NSString *string = [mInfoDict JSONRepresentation];
    NSLog(@"string :%@",string);
    [mHttpRequest CustomFormDataRequestDict:string tag:ClientSaveClassroomHTTPTAG url:[InstanceView FormatUrl:@"ClientSaveClassroom"]];

}
#pragma mark 学生考勤
-(void)StudentStatus:sender
{
    UIButton *mButton =(UIButton *)sender;
    [self performSelectorOnMainThread:@selector(SelectStudenStatus:) withObject:mButton waitUntilDone:YES];

}   
-(void)SelectStudenStatus:(UIButton*)mButton
{
    InstanceStudent *mInstanceStudent = [mStudents objectAtIndex:mButton.tag];
    
    if (mInstanceStudent.IsMark == NO) {
        [mButton setTitleColor:[UIColor redColor] forState:0];
        mInstanceStudent.IsMark = YES;
        [mButtonTitles addObject:mButton];
    }
    else {
        int mark = -1;
        [mButton setTitleColor:[UIColor blackColor] forState:0];
        mInstanceStudent.IsMark = NO;
        for (int i = 0; i<[mButtonTitles count]; i++) {
            UIButton *mTempButton = [mButtonTitles objectAtIndex:i];
            if (mTempButton == mButton) {
                mark = i;
            }

        }
        if (mark >=0) {
            [mButtonTitles removeObjectAtIndex:mark];
        }
    }
 
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initViewData];
    [self initView];
    
    NSLog(@"mDataDict %@,",[mDataDict JSONRepresentation]);
    
    if (IsHistory == NO) {
            mHttpRequest.Delegate = self;
            NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
            [mDict setObject:[mDataDict objectForKey:@"teacherId"] forKey:@"teacherId"];
            [mDict setObject:[mDataDict objectForKey:@"termId"] forKey:@"termId"];
            [mDict setObject:[NSNumber numberWithInt:mInstanceObject.gradeId] forKey:@"gradeId"];
            [mDict setObject:[NSNumber numberWithInt:mInstanceObject.weekId] forKey:@"weekId"];
            [mDict setObject:[NSNumber numberWithInt:mInstanceObject.sectionId] forKey:@"sectionId"];
            [mDict setObject:[NSNumber numberWithInt:mInstanceObject.subjectId] forKey:@"subjectId"];
            [mDataDict setObject:[NSNumber numberWithInt:mInstanceObject.gradeId] forKey:@"gradeId"];
            NSString *string = [mDict JSONRepresentation];
            [mDict release];
            
            [mHttpRequest CustomFormDataRequestDict:string tag:ClientClassroomHTTPTAG url:[InstanceView FormatUrl:@"ClientClassroom"]];
    }
    else {
            mHttpRequest.Delegate = self;
            NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
            [mDict setObject:[mDataDict objectForKey:@"Historyid"] forKey:@"id"];
            NSString *string = [mDict JSONRepresentation];
            [mDict release];
            
            [mHttpRequest CustomFormDataRequestDict:string tag:ClientClassroomHTTPTAG url:[InstanceView FormatUrl:@"ClientChooseClassroom"]];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];  
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];  

}
-(void)setButtonTitleColor:(UIButton*)mButton status:(int)mStatus
{
    switch (mStatus) {
        case 0:
            [mButton setTitleColor:[UIColor blackColor] forState:0];
            break;
        case 1:
            [mButton setTitleColor:HEXCOLOR(0xee5f00) forState:0];
            break;   
        case 2:
            [mButton setTitleColor:HEXCOLOR(0xe40a94) forState:0];
            break;   
        case 3:
            [mButton setTitleColor:HEXCOLOR(0x0095f3) forState:0];
            break;   
        default:
            [mButton setTitleColor:[UIColor blackColor] forState:0];
            break;
    }
}
-(void)addStudens:(NSArray *)seats
{
    [mStudents removeAllObjects];
    for (int i = 0; i<[seats count]; i++) {
        InstanceStudent *mInstanceStudent = [[InstanceStudent alloc]init];
        mInstanceStudent.status = [[NSString stringWithFormat:@"%@",[[seats objectAtIndex:i] valueForKey:@"status"]] intValue];
        mInstanceStudent.colsId = [[NSString stringWithFormat:@"%@",[[seats objectAtIndex:i] valueForKey:@"colsId"]] intValue];
        mInstanceStudent.rowsId = [[NSString stringWithFormat:@"%@",[[seats objectAtIndex:i] valueForKey:@"rowsId"]] intValue];
        mInstanceStudent.studentId = [[NSString stringWithFormat:@"%@",[[seats objectAtIndex:i] valueForKey:@"studentId"]] intValue];
        mInstanceStudent.studentName = [NSString stringWithFormat:@"%@",[[seats objectAtIndex:i] valueForKey:@"studentName"]];
        mInstanceStudent.gender = [NSString stringWithFormat:@"%@",[[seats objectAtIndex:i] valueForKey:@"gender"]];

        mInstanceStudent.IsMark = NO;
        [mStudents addObject:mInstanceStudent];
        [mInstanceStudent release];
    }
    [seats release];

    int count = [mStudents count];
    int MaxRows = 0 ;
    int MaxCols = 0;
    for (int i = 0 ; i <count; i++) {
        if (i%5 == 0) {
           UIImageView *imgageView =  [InstanceView addUIImageViewOfPoint:((UIScrollView*)[self.view viewWithTag:2001]) imageFile:@"单行线.png" point:CGPointMake(0,i/5*63)];
            imgageView.tag = 1212100+i;
        }
        InstanceStudent *mInstanceStudent = [mStudents objectAtIndex:i];
        if (MaxRows <= mInstanceStudent.rowsId) {
            MaxRows = mInstanceStudent.rowsId;
        }
        if (MaxCols <= mInstanceStudent.colsId) {
            MaxCols = mInstanceStudent.colsId;
        }
        CGPoint point = CGPointMake(8+48*(mInstanceStudent.colsId-1),63*(mInstanceStudent.rowsId-1));
        UIButton *mButton;
        if ([mInstanceStudent.gender isEqualToString:@"男"]) 
            mButton = [InstanceView addUIButtonByImageAndAutoSize:((UIScrollView*)[self.view viewWithTag:2001]) image:@"男同学.png" point:point];
        else
             mButton = [InstanceView addUIButtonByImageAndAutoSize:((UIScrollView*)[self.view viewWithTag:2001]) image:@"女同学.png" point:point];

        mButton.tag = i;
        [mButton setTitle:mInstanceStudent.studentName forState:0];
        [mButton setTitleEdgeInsets:UIEdgeInsetsMake(5, 0, -45, 0)];
        mButton.titleLabel.font = [UIFont systemFontOfSize:12];
        mButton.titleLabel.textAlignment = UITextAlignmentCenter;
        
        [self setButtonTitleColor:mButton status:mInstanceStudent.status];

        [mButton addTarget:self action:@selector(StudentStatus:) forControlEvents:64];
    }
    
    [((UIScrollView*)[self.view viewWithTag:2001]) setContentSize:CGSizeMake(8+48*(MaxCols),MaxRows* 63)];
}



-(void)addPlans:(NSArray *)Plans tag:(int)tag;
{
    
    NSMutableArray *arrays = [[NSMutableArray alloc]init];
    for (int i = 0; i<[Plans count]; i++) {
        PlanObject *mPlanObject = [[PlanObject alloc]init];
        mPlanObject.planId = [[NSString stringWithFormat:@"%@",[[Plans objectAtIndex:i] valueForKey:@"planId"]] intValue];
        mPlanObject.name = [NSString stringWithFormat:@"%@",[[Plans objectAtIndex:i] valueForKey:@"name"]];
        [arrays addObject:mPlanObject];
        [mPlanObject release];
    }
    switch (tag) {
        case ClientSeniorHTTPTAG:
            [self.planSeniors removeAllObjects];
            self.planSeniors = arrays;
            clientPlanFlag = ClientSeniorHTTPTAG;
            break;
            
        default:
            break;
    }
    
    NSLog(@"self.planSeniors  count = %d",[self.planSeniors count]);
    [mPlans removeAllObjects];
    

    
    [Plans release];
    
    UIView *mClassroomView = (UIView *)[self.view viewWithTag:1000011];

    UIImageView *SearchPlanView = [InstanceView addUIImageViewOfPoint:mClassroomView imageFile:@"选择教案背景.png" point:CGPointMake(0, 0)];
    SearchPlanView.tag = 20011;
    [SearchPlanView setUserInteractionEnabled:YES];
    
    
    planViewController *planViewCrtl = [[planViewController alloc]initWithNibName:@"planViewController" bundle:nil];
    
    self.navPlan = [[UINavigationController alloc] initWithRootViewController:planViewCrtl];
    [self.navPlan setNavigationBarHidden:YES];
    [self.navPlan.view setFrame:CGRectMake(360, 117, 604, 578)];
    
    [self.view addSubview:self.navPlan.view];
//    
//    UITableView *tableview = [[UITableView alloc]initWithFrame:CGRectMake(21, 101, 566, 435)];
//    [tableview setBackgroundColor:[UIColor clearColor]];
//    tableview.delegate = self;
//    tableview.dataSource = self;
//    [SearchPlanView addSubview:tableview];
//    [tableview release];

}
-(void)addFetchEbooks:(NSArray *)fetchbooks
{
    if ([fetchbooks count] == 0) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"" message:@"暂无电子书" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
        [alertView release];
        [fetchbooks release];
        return;
    }
    [mFetchEbooks removeAllObjects];
    for (int i = 0; i<[fetchbooks count]; i++) {
        FetchEbook *mFetchEbook = [[FetchEbook alloc]init];
        mFetchEbook.name = [NSString stringWithFormat:@"%@",[[fetchbooks objectAtIndex:i] valueForKey:@"name"]];
        mFetchEbook.minUrl = [NSString stringWithFormat:@"%@",[[fetchbooks objectAtIndex:i] valueForKey:@"minUrl"]];
        mFetchEbook.url = [NSString stringWithFormat:@"%@",[[fetchbooks objectAtIndex:i] valueForKey:@"url"]];
        mFetchEbook.image = nil;
        mFetchEbook.isLoad = NO;
        [mFetchEbooks addObject:mFetchEbook];
        [mFetchEbook release];
    }
    [fetchbooks release];
    
    if ([mFetchEbooks count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"没有电子书" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    NSLog(@"mEBooks count = %d",[mFetchEbooks count]);

    FetchEbookViewController *mSearchClassroom = [[FetchEbookViewController alloc]initWithNibName:@"FetchEbookViewController" bundle:nil];
    [mSearchClassroom.view setBackgroundColor:[UIColor blackColor]];
    mSearchClassroom.mEBooks = mFetchEbooks;
    NSLog(@"keyWindow.view .frame %f , %f",[UIApplication sharedApplication].keyWindow.rootViewController.view.frame.size.width ,[UIApplication sharedApplication].keyWindow.rootViewController.view.frame.size.height);
    [mSearchClassroom.view setFrame:[[UIScreen mainScreen] bounds]];
    [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:mSearchClassroom.view];
    
}
-(void)addButtons:(NSArray*)buttons Touch:(BOOL)IsTouch;
{
    [mButtons removeAllObjects];
    for (int i = 0 ; i<[buttons count]; i++) {
        
        ButtonObject *mButtonObject = [[ButtonObject alloc]init];
        mButtonObject.mID = [[NSString stringWithFormat:@"%@",[[buttons objectAtIndex:i] valueForKey:@"id"]] intValue];
        mButtonObject.name = [NSString stringWithFormat:@"%@",[[buttons objectAtIndex:i] valueForKey:@"name"]];
        mButtonObject.url = [NSString stringWithFormat:@"%@",[[buttons objectAtIndex:i] valueForKey:@"url"]];
        [mButtons addObject:mButtonObject];
        [mButtonObject release];
    }
    [buttons release];

    UIView *mClassroomView = (UIView *)[self.view viewWithTag:1000011];

    UIScrollView *scroller = [[UIScrollView alloc]initWithFrame:CGRectMake(120,429, 460, 30)];
    [scroller setBackgroundColor:[UIColor clearColor]];
    [scroller setUserInteractionEnabled:IsTouch];
    [mClassroomView addSubview:scroller];
    [scroller release];
    
    UIFont *font = [UIFont systemFontOfSize:14];
    int PointX = 0;
    
    
    for (int i = 0 ; i<[mButtons count]; i++) {
        
        ButtonObject *mButtonObject = [mButtons objectAtIndex:i];
        
        CGSize size = [mButtonObject.name sizeWithFont:font];
        
        [InstanceView addUIImageView:scroller imageFile:[self getFileType:mButtonObject.url] rect:CGRectMake(PointX+5, 3, 24, 26)];

        UIButton * mButton = [InstanceView addUIButtonByImage:scroller image:@"nil" rect:CGRectMake(PointX+30, 3, size.width, scroller.frame.size.height)];
        mButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [mButton setShowsTouchWhenHighlighted:YES];
        [mButton setTitleColor:[UIColor blackColor] forState:0];
        [mButton setTitle:mButtonObject.name forState:0];
        [mButton addTarget:self action:@selector(OpenAttachment:) forControlEvents:64];
        mButton.tag = i;
        PointX = mButton.frame.origin.x + mButton.frame.size.width +10;
    }
    [scroller setContentSize:CGSizeMake(PointX, 0)];
}
-(NSString *)getFileType:(NSString *)url
{
    if ([url rangeOfString:@"doc"].location != NSNotFound || [url rangeOfString:@"docx"].location != NSNotFound) {
        return @"icon_doc.png";
    }else if ([url rangeOfString:@"xls"].location != NSNotFound ){
        return @"icon_xls.png";
    }else if ([url rangeOfString:@"ppt"].location != NSNotFound ){
        return @"icon_ppt.png";
        
    }else if ([url rangeOfString:@"mp3"].location != NSNotFound ){
        return @"icon_mp3.png";
    }else if ([url rangeOfString:@"txt"].location != NSNotFound ){
        return @"icon_txt.png";
    }
    
    return nil;
    
}
-(void)addButtonsOfBigView:(NSArray*)buttons view:(UIView*)mTempView Touch:(BOOL)IsTouch;
{
    [mButtons removeAllObjects];
    for (int i = 0 ; i<[buttons count]; i++) {
        ButtonObject *mButtonObject = [[ButtonObject alloc]init];
        mButtonObject.mID = [[NSString stringWithFormat:@"%@",[[buttons objectAtIndex:i] valueForKey:@"id"]] intValue];
        mButtonObject.name = [NSString stringWithFormat:@"%@",[[buttons objectAtIndex:i] valueForKey:@"name"]];
        mButtonObject.url = [NSString stringWithFormat:@"%@",[[buttons objectAtIndex:i] valueForKey:@"url"]];
        [mButtons addObject:mButtonObject];
        [mButtonObject release];
    }
    [buttons release];
    
    
    UIScrollView *scroller = [[UIScrollView alloc]initWithFrame:CGRectMake(224, 230+63+66+60+80, 680, 35)];
    [scroller setBackgroundColor:[UIColor clearColor]];
    [scroller setUserInteractionEnabled:IsTouch];
    [mTempView addSubview:scroller];
    [scroller release];
    
    UIFont *font = [UIFont systemFontOfSize:14];
    int PointX = 0;
    
    
    for (int i = 0 ; i<[mButtons count]; i++) {
        ButtonObject *mButtonObject = [mButtons objectAtIndex:i];
        CGSize size = [mButtonObject.name sizeWithFont:font];
        UIButton * mButton = [InstanceView addUIButtonByImage:scroller image:@"nil" rect:CGRectMake(PointX+5, 0, size.width, scroller.frame.size.height)];
        mButton.titleLabel.font = [UIFont systemFontOfSize:12];
        [mButton setShowsTouchWhenHighlighted:YES];
        [mButton setTitleColor:[UIColor blackColor] forState:0];
        [mButton setTitle:mButtonObject.name forState:0];
        [mButton addTarget:self action:@selector(OpenAttachment:) forControlEvents:64];
        mButton.tag = i;
        PointX = mButton.frame.origin.x + mButton.frame.size.width +10;
    }
    [scroller setContentSize:CGSizeMake(PointX, 0)];
}

#pragma mark 打开附件
-(void)OpenAttachment:sender
{
    UIButton *mBtn = (UIButton *)sender;
    ButtonObject *mButtonObject = [mButtons objectAtIndex:mBtn.tag];
    
    UIWebView *mWebView = [[UIWebView alloc]initWithFrame:self.view.frame];
    [mWebView setUserInteractionEnabled:YES];
    [mWebView setScalesPageToFit:YES];
    [[UIApplication sharedApplication].keyWindow.rootViewController.view addSubview:mWebView];
    NSLog(@"附件 :%@",mButtonObject.url);
    mWebView.tag = 40001;
    NSURL *mUrl = [NSURL URLWithString:mButtonObject.url];
    NSURLRequest *mRequest = [NSURLRequest requestWithURL:mUrl];
    [mWebView loadRequest:mRequest];    
    [mWebView release];
    
    UIButton *mCloseButton = [InstanceView addUIButtonByImageAndAutoSize:[UIApplication sharedApplication].keyWindow.rootViewController.view image:@"叉.png" point:CGPointMake(959, 0)];
    mCloseButton.tag = 40002;
    [mCloseButton addTarget:self action:@selector(CloseView) forControlEvents:64];
}
-(void)CloseView
{
    [[[UIApplication sharedApplication].keyWindow.rootViewController.view viewWithTag:40001] removeFromSuperview];
    [[[UIApplication sharedApplication].keyWindow.rootViewController.view viewWithTag:40002] removeFromSuperview];

}

-(void)handleTwoTapFrom:(UITapGestureRecognizer *)singGesture
{
    if (isMaxWebView) {
        return;
    }
    NSLog(@"dd");
    UIView *mClassroomView = (UIView *)[self.view viewWithTag:1000011];
    [mClassroomView setFrame:CGRectMake(0, 0, 1024, 748)];
    [self.view bringSubviewToFront:mClassroomView];
    UIWebView *mWebView = (UIWebView *)[mClassroomView viewWithTag:606060];
    [mWebView setFrame:CGRectMake(0, 0, 1024, 748)];
    [mClassroomView bringSubviewToFront:mWebView];

    UIButton *mCloseButton = [InstanceView addUIButtonByImageAndAutoSize:[UIApplication sharedApplication].keyWindow.rootViewController.view image:@"叉.png" point:CGPointMake(959, 0)];
    mCloseButton.tag = 40002;
    [mCloseButton addTarget:self action:@selector(CloseWebView:) forControlEvents:64];

    isMaxWebView = YES;
}
-(void)handleTwoTapFromOfBigView:(UITapGestureRecognizer *)singGesture
{
    if (isMaxWebView) {
        return;
    }
    UIView *mClassroomView = (UIView *)[self.view viewWithTag:12312321];
    UIWebView *mWebView = (UIWebView *)[mClassroomView viewWithTag:606061];
    [mWebView setFrame:CGRectMake(0, 0, 1024, 748)];
    [mClassroomView bringSubviewToFront:mWebView];

    UIButton *mCloseButton = [InstanceView addUIButtonByImageAndAutoSize:[UIApplication sharedApplication].keyWindow.rootViewController.view image:@"叉.png" point:CGPointMake(959, 0)];
    [mCloseButton addTarget:self action:@selector(CloseWebViewBig:) forControlEvents:64];
    isMaxWebView = YES;

}
#pragma mark 关闭小教学过程
-(void)CloseWebView:sender
{
    UIButton *mTempButton = sender;
    [mTempButton removeFromSuperview];
    UIView *mClassroomView = (UIView *)[self.view viewWithTag:1000011];
    UIImage *image = [UIImage imageNamed:@"教案背景.png"];
    [mClassroomView setFrame:CGRectMake(360, 117, image.size.width,image.size.height)];
    UIWebView *mWebView = (UIWebView *)[mClassroomView viewWithTag:606060];
    [mWebView setFrame:CGRectMake(118, 154+63+66+63, 466, 82)];
    isMaxWebView = NO;
}
-(void)CloseWebViewBig:sender
{
    UIButton *mTempButton = sender;
    [mTempButton removeFromSuperview];
    UIView *mClassroomView = (UIView *)[self.view viewWithTag:12312321];
    UIWebView *mWebView = (UIWebView *)[mClassroomView viewWithTag:606061];
    [mWebView setFrame:CGRectMake(224, 230+63+66+60, 680, 80)];
    isMaxWebView = NO;

}
#pragma mark 教案全屏
-(void)SetPlansScreen
{
    NSString *ResultContent = [mInfoDict objectForKey:@"ResultContent"];
    UIView *mView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 1024, 748)];
    [InstanceView addUIImageViewOfPoint:mView imageFile:@"主界面教案背景.png" point:CGPointMake(0,0)];
    mView.tag = 12312321;
    
    [InstanceView addLabelOfScroll:mView rect:CGRectMake(335, 140, 570, 30) string:[[ResultContent JSONValue] objectForKey:@"target1"] textSize:14 textAlignment:UITextAlignmentLeft];
    [InstanceView addLabelOfScroll:mView rect:CGRectMake(335, 140+30, 570, 30) string:[[ResultContent JSONValue] objectForKey:@"target2"] textSize:14 textAlignment:UITextAlignmentLeft];
    [InstanceView addLabelOfScroll:mView rect:CGRectMake(335, 140+30*2, 570, 30) string:[[ResultContent JSONValue] objectForKey:@"target3"] textSize:14 textAlignment:UITextAlignmentLeft];

    
    [InstanceView addUITextView:mView nnstring:[[ResultContent JSONValue] objectForKey:@"teachFocal"] rect:CGRectMake(225, 230, 680, 60)];             // 教案教学重点
    [InstanceView addUITextView:mView nnstring:[[ResultContent JSONValue] objectForKey:@"teachDefficulty"] rect:CGRectMake(225, 230+63, 680, 60)];     //教案教学难点1
    [InstanceView addUITextView:mView nnstring:[[ResultContent JSONValue] objectForKey:@"teachPlan"] rect:CGRectMake(225, 230+63+66, 680, 60)];        //教案教学安排

    {
        UIWebView *mWebView = [InstanceView addUIWebView:mView string:[[ResultContent JSONValue] objectForKey:@"teachProcess"] rect:CGRectMake(224, 230+63+66+60, 680, 80)];
        mWebView.tag = 606061;
        UITapGestureRecognizer *mWebGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTwoTapFromOfBigView:)];
        mWebGesture.delegate = self;
        mWebGesture.numberOfTapsRequired = 1; // 单击
        mWebGesture.cancelsTouchesInView = NO;
        [mWebView addGestureRecognizer:mWebGesture];
        [mWebGesture release];
        isMaxWebView = NO;
    }
    [self addButtonsOfBigView:[[[ResultContent JSONValue] valueForKey:@"attachment"] retain] view:mView  Touch:YES];

    
    int planId  = [[[ResultContent JSONValue] objectForKey:@"planId"] intValue];
    if (planId==0) {
        [InstanceView addTextFileldOfScroll:mView rect:CGRectMake(225, 230+63+66+60+80+50, 680, 30) string:[[ResultContent JSONValue] objectForKey:@"reflect"] placeholder:@"" tag:3330111];
        [InstanceView addTextFileldOfScroll:mView rect:CGRectMake(225, 230+63+66+60+80+80, 680, 30) string:[[ResultContent JSONValue] objectForKey:@"extend"] placeholder:@"" tag:3330222];
        [self addButtons:[[[ResultContent JSONValue] valueForKey:@"attachment"] retain] Touch:NO];
    }
    else {
        UITextField *textfiel1 = [InstanceView addTextFileldOfScroll:mView rect:CGRectMake(225,230+63+66+60+80+50, 680, 35) string:[[ResultContent JSONValue] objectForKey:@"reflect"] placeholder:@"" tag:0];     //教案反思
        textfiel1.tag = 3330111;
        textfiel1.delegate = self;
        [textfiel1 setEnabled:YES];
        UITextField *textfiel2 =  [InstanceView addTextFileldOfScroll:mView rect:CGRectMake(225, 230+63+66+60+80+80, 680, 35) string:[[ResultContent JSONValue] objectForKey:@"extend"] placeholder:@"" tag:0];//教案延伸
        [textfiel2 setEnabled:YES];
        textfiel2.tag = 3330222;
        textfiel2.delegate = self;
        
        //------ 
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:mView image:@"保存.png" point:CGPointMake(940,58)];
        [mButton addTarget:self action:@selector(SavaPlan) forControlEvents:64];
        
    }

    {
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:mView image:@"返回.png" point:CGPointMake(29, 10)];
        [mButton addTarget:self action:@selector(SetBackSmartView) forControlEvents:64];
    }
    [self.view addSubview:mView];
    [mView release];
}
-(void)SetBackSmartView
{
    [((UIView *)[self.view viewWithTag:12312321]) removeFromSuperview];
}
-(void)WriteTeahPlan:(NSString*)ResultContent
{
 
    UIView *mClassroomView = (UIView *)[self.view viewWithTag:1000011];

    [InstanceView addLabelOfViewRect:self.view rect:CGRectMake(330, 53, 666, 40) string:[[ResultContent JSONValue] objectForKey:@"name"] textSize:16 textAlignment:UITextAlignmentCenter];
    [InstanceView addLabelOfView:mClassroomView point:CGPointMake(55, 42) string:[[ResultContent JSONValue] objectForKey:@"subjectName"] textSize:14 textAlignment:UITextAlignmentLeft tag:1001];
    [InstanceView addLabelOfView:mClassroomView point:CGPointMake(220, 42) string:[[ResultContent JSONValue] objectForKey:@"gradeName"] textSize:14 textAlignment:UITextAlignmentLeft tag:1002];
    [InstanceView addLabelOfView:mClassroomView point:CGPointMake(405, 42) string:[[ResultContent JSONValue] objectForKey:@"date"] textSize:14 textAlignment:UITextAlignmentLeft tag:1003];

    
    [InstanceView addLabelOfScroll:mClassroomView rect:CGRectMake(196, 68, 390, 28) string:[[ResultContent JSONValue] objectForKey:@"target1"] textSize:14 textAlignment:UITextAlignmentLeft];
    [InstanceView addLabelOfScroll:mClassroomView rect:CGRectMake(196, 68+28, 390, 28) string:[[ResultContent JSONValue] objectForKey:@"target2"] textSize:14 textAlignment:UITextAlignmentLeft];
    [InstanceView addLabelOfScroll:mClassroomView rect:CGRectMake(196, 68+28*2, 390, 28) string:[[ResultContent JSONValue] objectForKey:@"target3"] textSize:14 textAlignment:UITextAlignmentLeft];
    
    [InstanceView addUITextView:mClassroomView nnstring:[[ResultContent JSONValue] objectForKey:@"teachFocal"] rect:CGRectMake(118, 154, 468, 63)];             // 教案教学重点
    [InstanceView addUITextView:mClassroomView nnstring:[[ResultContent JSONValue] objectForKey:@"teachDefficulty"] rect:CGRectMake(118, 154+63, 468, 66)];     //教案教学难点1
    [InstanceView addUITextView:mClassroomView nnstring:[[ResultContent JSONValue] objectForKey:@"teachPlan"] rect:CGRectMake(118, 154+63+66, 468, 61)];        //教案教学安排
    {
        UIWebView *mWebView = [InstanceView addUIWebView:mClassroomView string:[[ResultContent JSONValue] objectForKey:@"teachProcess"] rect:CGRectMake(118, 154+63+66+63, 466, 82)];
        mWebView.tag = 606060;
        UITapGestureRecognizer *mWebGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTwoTapFrom:)];
        mWebGesture.delegate = self;
        mWebGesture.numberOfTapsRequired = 1; // 单击
        mWebGesture.cancelsTouchesInView = NO;
        [mWebView addGestureRecognizer:mWebGesture];
        [mWebGesture release];
        isMaxWebView = NO;
    }
    UIButton *mButtonSearchPlan = [InstanceView addUIButtonByImageAndAutoSize:mClassroomView image:@"教案按钮.png" point:CGPointMake(28, 120)];
    mButtonSearchPlan.tag = 101010101;
    [mButtonSearchPlan addTarget:self action:@selector(SearchPlan) forControlEvents:64];


}
#pragma mark - UIGestureRecognizerDelegate
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    NSLog(@"tag :%d",tag);
    if (tag/BACKRESULTCODE == 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:ResultContent delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alert.tag = tag;
        alert.delegate = self;
        [alert show];
        [alert release];
        return;
    }
    if (tag == ClientClassroomHTTPTAG) {
        for (UIView *sub in [self.view subviews]) {
            [sub removeFromSuperview];
        }
        [self initView];
        // classroom id 
        mClassroomID = [[[ResultContent JSONValue] objectForKey:@"id"] intValue];
        
        [mInfoDict setObject:[[ResultContent JSONValue] objectForKey:@"id"]  forKey:@"id"];
        [mInfoDict setObject:[[ResultContent JSONValue] objectForKey:@"planId"]  forKey:@"planId"];
        [mInfoDict setObject:ResultContent forKey:@"ResultContent"];
        
        [mDataDict setObject:[[ResultContent JSONValue] objectForKey:@"id"] forKey:@"id"];
        [mDataDict setObject:[[ResultContent JSONValue] objectForKey:@"name"] forKey:@"name"];

        //---- 科目，班级

        //------------ 学生
        [self addStudens:[[[ResultContent JSONValue] valueForKey:@"seat"] retain]];
        [self WriteTeahPlan:ResultContent];

        
        UIView *mClassroomView = (UIView *)[self.view viewWithTag:1000011];

        int planId  = [[[ResultContent JSONValue] objectForKey:@"planId"] intValue];
        if (planId==0) {
            [InstanceView addTextFileldOfScroll:mClassroomView rect:CGRectMake(120, 469, 466, 30) string:[[ResultContent JSONValue] objectForKey:@"reflect"] placeholder:@"" tag:3330111];
            [InstanceView addTextFileldOfScroll:mClassroomView rect:CGRectMake(120, 499, 466, 30) string:[[ResultContent JSONValue] objectForKey:@"extend"] placeholder:@"" tag:3330222];
            [self addButtons:[[[ResultContent JSONValue] valueForKey:@"attachment"] retain] Touch:NO];
        }
        else {
            UITextField *textfiel1 = [InstanceView addTextFileldOfScroll:mClassroomView rect:CGRectMake(120,469, 468, 35) string:[[ResultContent JSONValue] objectForKey:@"reflect"] placeholder:@"" tag:0];     //教案反思
            textfiel1.tag = 3330111;
            textfiel1.delegate = self;
            [textfiel1 setEnabled:YES];
            UITextField *textfiel2 =  [InstanceView addTextFileldOfScroll:mClassroomView rect:CGRectMake(120, 499, 468, 35) string:[[ResultContent JSONValue] objectForKey:@"extend"] placeholder:@"" tag:0];//教案延伸
            [textfiel2 setEnabled:YES];
            textfiel2.tag = 3330222;
            textfiel2.delegate = self;
            
            //------ 
            [self addButtons:[[[ResultContent JSONValue] valueForKey:@"attachment"] retain] Touch:YES];
            UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:mClassroomView image:@"保存.png" point:CGPointMake(540, 5)];
            [mButton addTarget:self action:@selector(SavaPlan) forControlEvents:64];
            self.currentBookButton.hidden = NO;
        }
        return;
        //-------------- 选择教按
    }
    /// ----- 选 择 教案 列表
    if (tag == ClientSearchPlanHTTPTAG || tag == ClientSubjectHTTPTAG || tag == ClientSeniorHTTPTAG) {
        if (((UIImageView*)[self.view viewWithTag:20011])!=nil) {
            [(UIImageView*)[self.view viewWithTag:20011] removeFromSuperview];
        }
        NSLog(@"ResultContent = %@,",ResultContent);
        [self addPlans:[[ResultContent JSONValue] retain] tag:tag];
        return;

    }
    /// ----- 历史 教案 列表

    if (tag == ClientChoosePlanHTTPTAG) {
        [self.navPlan.view removeFromSuperview];
        
        NSLog(@"选择教按");
        UIView *mClassroomView = (UIView *)[self.view viewWithTag:1000011];

        for (UIView *sub in [mClassroomView subviews]) {
            if (sub.tag == 1001 || sub.tag == 1002 || sub.tag == 1003) {
                continue;
            }
            [sub removeFromSuperview];
        }
        if (mClassroomView == nil) {
            NSLog(@"空");
            return;
        }
        [mInfoDict setObject:[[ResultContent JSONValue] objectForKey:@"planId"]  forKey:@"planId"];
        [mInfoDict setObject:ResultContent forKey:@"ResultContent"];

        [self WriteTeahPlan:ResultContent];
        [self addButtons:[[[ResultContent JSONValue] valueForKey:@"attachment"] retain] Touch:YES];

        
        UITextField *textfiel1 = [InstanceView addTextFileldOfScroll:mClassroomView rect:CGRectMake(120, 469, 468, 35) string:[[ResultContent JSONValue] objectForKey:@"reflect"] placeholder:@"" tag:0];     //教案反思
        textfiel1.tag = 3330111;
        textfiel1.delegate = self;
        [textfiel1 setEnabled:YES];
        UITextField *textfiel2 =  [InstanceView addTextFileldOfScroll:mClassroomView rect:CGRectMake(120, 499, 468, 35) string:[[ResultContent JSONValue] objectForKey:@"extend"] placeholder:@"" tag:0];//教案延伸
        [textfiel2 setEnabled:YES];
        textfiel2.tag = 3330222;
        textfiel2.delegate = self;
        
        //------ 
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:mClassroomView image:@"保存.png" point:CGPointMake(540, 5)];
        [mButton addTarget:self action:@selector(SavaPlan) forControlEvents:64];
        

        NSLog(@"mInfoDict :%@",[mInfoDict JSONRepresentation]);
        self.currentBookButton.hidden = NO;
        return;

    }
    if (tag == ClientSaveClassroomHTTPTAG) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"保存教案成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;

    }
    if (AttendanceHTTPTAG == tag) {
        [self RelodateOfStudents];
        return;

    }
    if (FetchEbookHTTPTAG == tag) { //电子书
        [self addFetchEbooks:[[[ResultContent JSONValue] valueForKey:@"ebook"] retain]];

        return;

    }
    if (ClientSubjectHTTPTAG ==  tag) {
        NSLog(@"选择科目");
    }
}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"网络错误" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    alert.tag = tag;
    alert.delegate = self;
    [alert show];
    [alert release];

}

#pragma mark 2.7	搜索教案
-(void)SearchPlan
{
    planViewController *planViewCrtl = [[planViewController alloc]initWithNibName:@"planViewController" bundle:nil];
    planViewCrtl.cleintRequest = @"ClientVersion";
    planViewCrtl.subjectId = mInstanceObject.subjectId;
    planViewCrtl.seniorId = mInstanceObject.seniorId;
    planViewCrtl.titleString = @"请选择版本";
    self.navPlan = [[UINavigationController alloc] initWithRootViewController:planViewCrtl];
    [self.navPlan setNavigationBarHidden:YES];
    [self.navPlan.view setFrame:CGRectMake(360, 117, 604, 578)];
    
    [self.view addSubview:self.navPlan.view];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateSendQuesttionUserProfileNotification:)
                                                 name:NOTIFICATION_PLAN
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closeNavPlanNotification:)
                                                 name:NOTIFICATION_CLOSEPLAN
                                               object:nil];
    
    return;
//    NSLog(@"mDataDict--- :%@",[mDataDict JSONRepresentation]);
//    mHttpRequest.Delegate = self;
//    NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
//    [mDict setObject:[mDataDict objectForKey:@"termId"] forKey:@"termId"];
//    [mDict setObject:[NSNumber numberWithInt:mInstanceObject.gradeId] forKey:@"gradeId"];
//    [mDict setObject:[NSNumber numberWithInt:mInstanceObject.subjectId] forKey:@"subjectId"];
//    
//    NSString *string = [mDict JSONRepresentation];
//    [mDict release];
//    
//    [mHttpRequest CustomFormDataRequestDict:string tag:ClientSearchPlanHTTPTAG url:[InstanceView FormatUrl:@"ClientSearchPlan"]];

    
    [mHttpRequest CustomFormDataRequestDict:@"" tag:ClientSeniorHTTPTAG url:[InstanceView FormatUrl:@"ClientSenior"]];

}
#pragma mark 响应查询教案
- (void) updateSendQuesttionUserProfileNotification:(NSNotification *) notification
{
    NSLog(@"查询教案");
    NSMutableDictionary *dict = (NSMutableDictionary*) [[NSUserDefaults standardUserDefaults] objectForKey:NOTIFICATION_QUESTPLAN];
    
    NSLog(@"选择教案 = %@",dict);
    
    
    NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
    [mDict setObject:[NSNumber numberWithInt:mClassroomID ] forKey:@"id"];
    [mDict setObject:[dict objectForKey:@"planId"] forKey:@"planId"];
    
    NSString *string = [mDict JSONRepresentation];
    [mDict release];
    
    [mHttpRequest CustomFormDataRequestDict:string tag:ClientChoosePlanHTTPTAG url:[InstanceView FormatUrl:@"ClientChoosePlan"]];

    catalogId = [[dict objectForKey:@"catalogId"] intValue];
    
}
#pragma mark


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *cellName = @"cellstr";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    
    if (cell== nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
        cell.textLabel.text = @"";
        
    }
    PlanObject *mPlanObject = nil;
    switch (clientPlanFlag) {
        case ClientSeniorHTTPTAG:
            mPlanObject = [self.planSeniors objectAtIndex:[indexPath row]];

            break;
            
        default:
            break;
    }
    cell.textLabel.text = mPlanObject.name;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return [mPlans count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TableviewHeight;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    PlanObject *mPlanObject = [mPlans objectAtIndex:[indexPath row]];

    
    mHttpRequest.Delegate = self;
    NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
    [mDict setObject:[NSNumber numberWithInt:mClassroomID ] forKey:@"id"];
    [mDict setObject:[NSNumber numberWithInt:mPlanObject.planId] forKey:@"planId"];
    
    NSString *string = [mDict JSONRepresentation];
    [mDict release];
    
    [mHttpRequest CustomFormDataRequestDict:string tag:ClientChoosePlanHTTPTAG url:[InstanceView FormatUrl:@"ClientChoosePlan"]];

}
-(void)Seting
{
    
 }

-(void)MoveSelfView:(BOOL)ShowKeyBoard
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3f];
    CGRect rect = self.view.frame;
    if (ShowKeyBoard) {
        rect.origin.y = -300;
        [self.view setFrame:rect];
    }
    else {
        rect.origin.y = 0;
        [self.view setFrame:rect];
    }
    [UIView commitAnimations];
}
- (void)keyboardWillShow:(NSNotification *)notification {  
    [self MoveSelfView:YES];
}
- (void)keyboardWillHide:(NSNotification *)notification {  
    [self MoveSelfView:NO];
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField;              // called when 'return' key pressed. return NO to ignore.
{
    [self MoveSelfView:NO];
    [textField resignFirstResponder];
    return YES;
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag%BACKRESULTCODE == 100) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    if (alertView.tag == ClientClassroomHTTPTAG) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    else {
        return;
    }
}
-(void)dealloc
{
    
    //mStudents,*mPlans,*mButtons,*mAttendances,*mFetchEbooks,*mButtonTitles;
    

    [mStudents release];
    [mPlans release];
    [mButtons release];
    
    [mAttendances release];
    [mFetchEbooks release];
    [mButtonTitles release];
    
    [mDataDict release];
    [mInstanceObject release];
    
    [mInfoDict release];
    [mHttpRequest release];
    [super dealloc];	
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;

    self.mStudents = nil;
    self.mPlans = nil;
    self.mButtons = nil;
    
    self.mAttendances = nil;
    self.mButtonTitles = nil;
    self.mFetchEbooks = nil;

    
    self.mDataDict = nil;
    self.mInstanceObject = nil;
    
    self.mInfoDict = nil;
    self.mHttpRequest = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ( interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown ) {
        return YES;
    }
    else {
        return NO;
    }
}

@end
