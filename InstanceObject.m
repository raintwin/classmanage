//
//  InstanceObject.m
//  DMPK
//
//  Created by jiaodian on 12-10-30.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "InstanceObject.h"

#pragma mark ----------- InstanceObject

@implementation InstanceObject
@synthesize adjDate,gradeName,isDefault,isSingleWeek,subjectName;
@synthesize gradeId,sectionId,subjectId,weekId,seniorId;
-(void)dealloc
{
    [adjDate release]; adjDate = nil;
    [gradeName release]; gradeName = nil;
    [isDefault release]; isDefault = nil;
    [isSingleWeek release]; isSingleWeek = nil;
    [subjectName release]; subjectName = nil;

    [super dealloc];
}
@end

#pragma mark ----------- InstanceStudent

@implementation InstanceStudent
@synthesize colsId,rowsId,studentId,status,getderCode;
@synthesize studentName,gender;
@synthesize IsMark;
-(void)dealloc
{
    [studentName release];  studentName = nil;
    [gender release];       gender = nil;
    [super dealloc];
}
@end


#pragma mark ----------- PlanObject

@implementation PlanObject

@synthesize name,subjectName,teacherName;
@synthesize planId;
-(void)dealloc
{
    [name release];        name = nil;
    [subjectName release]; subjectName = nil;
    [teacherName release]; teacherName = nil;
    [super dealloc];
}

@end



#pragma mark ----------- ButtonObject
@implementation ButtonObject

@synthesize name,url;
@synthesize mID;
-(void)dealloc
{
    [name release]; name = nil;
    [url release]; url = nil;
    [super dealloc];
}

@end


@implementation StudentEvaluateMain
@synthesize evaluateMainId,studentId,totalScore;
@synthesize studentName,comment;
@synthesize IsMark;
-(void)dealloc
{
    [studentName release];  studentName = nil;
    [comment release];       comment = nil;
    [super dealloc];
}
@end


@implementation StudentEvaluateDetail
@synthesize itemId,evaluateId,score,evaluateScore;
@synthesize itemDescription,description;
@synthesize IsMark;
-(void)dealloc
{
    [itemDescription release];  itemDescription = nil;
    [description release];       description = nil;
    [super dealloc];
}
@end


@implementation FetchEbook
@synthesize name,minUrl,url;
@synthesize image;
@synthesize isLoad;
-(void)dealloc
{
    [name release];         name = nil;
    [minUrl release];       minUrl = nil;
    [url release];          url = nil;
    [image release];        image = nil;
    [super dealloc];
}
@end

