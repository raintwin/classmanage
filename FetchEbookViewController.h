//
//  FetchEbookViewController.h
//  DMPK
//
//  Created by jiaodian on 12-11-9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DownLoadTableImage.h"
#import "AppDelegate.h"

@interface FetchEbookViewController : UIViewController<HttpFormatRequestDeleagte,DownLoadImageOfScrollDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITableViewDelegate,UITableViewDataSource>
{
    int pageIndex,kNumberOfPages;
    CGRect mainScreen;
}
-(void)FinishDownLoadImageOfScrooll:(int)index Image:(UIImage*)image IsFailed:(BOOL)isFaild;

@property(nonatomic,assign)int planId;
@property(nonatomic,assign)BOOL isBook;

@property(nonatomic,retain)NSMutableArray *mEBooks;
@property(nonatomic,retain)NSMutableDictionary *mDownLoadImageDict;



@end
