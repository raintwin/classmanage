//
//  StudentEvaluateMainController.m
//  DMPK
//
//  Created by jiaodian on 12-11-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "StudentEvaluateMainController.h"
#import "InstanceObject.h"
#define TableviewHeight 38
@interface StudentEvaluateMainController ()

@end

@implementation StudentEvaluateMainController
@synthesize mDataDict;
@synthesize mStudents;
@synthesize mHttpRequest;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark 返回
-(void)SetBackNav
{
    [self.navigationController popViewControllerAnimated:YES];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [InstanceView addUIImageViewOfPoint:self.view imageFile:@"查看历史学生评价x.png" point:CGPointMake(0,0)];
    [InstanceView addLabelOfViewRect:self.view rect:CGRectMake(30, 53, 960, 40) string:[mDataDict objectForKey:@"name"] textSize:17 textAlignment:UITextAlignmentCenter];

    {
        NSMutableArray *array = [[NSMutableArray alloc]init];
        self.mStudents = array;
        [array release];
    }
    {
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"返回.png" point:CGPointMake(29, 10)];
        [mButton addTarget:self action:@selector(SetBackNav) forControlEvents:64];
    }
    {
        UILabel *label = [InstanceView addLabelOfViewRect:self.view rect:CGRectMake(765, 588, 166, 60) string:@"" textSize:15 textAlignment:UITextAlignmentLeft];
        label.tag = 10001;
    }
    {
        
        HttpFormatRequest *http = [[HttpFormatRequest alloc]init];
        http.Delegate = self;
        self.mHttpRequest = http;
        [http release];
        [self.view addSubview:mHttpRequest];
        
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:[mDataDict objectForKey:@"teacherId"] forKey:@"teacherId"];
        [mDict setObject:[mDataDict objectForKey:@"id"] forKey:@"id"];
        
        
        NSString *string = [mDict JSONRepresentation];
        [mDict release];
        [mHttpRequest CustomFormDataRequestDict:string tag:0 url:[InstanceView FormatUrl:@"ClientStudentEvaluateMain"]];
    }
    UITableView *mTableView = [[UITableView alloc]initWithFrame:CGRectMake(103, 172, 824, 416) style:UITableViewStylePlain];
    [mTableView setBackgroundColor:[UIColor clearColor]];
    //    [mTableView setSeparatorStyle:UITableViewCellAccessoryNone];
    mTableView.delegate = self;
    mTableView.dataSource  = self;
    mTableView.tag = 100111;
    [self.view addSubview:mTableView];
    [mTableView release];
}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    NSArray *students = [[[ResultContent JSONValue] valueForKey:@"evaluate"] retain];
    int score = 0;
    for (int i = 0; i<[students count]; i++) {
        StudentEvaluateMain *mStudentEvaluateDetail = [[StudentEvaluateMain alloc]init];
        mStudentEvaluateDetail.evaluateMainId = [[NSString stringWithFormat:@"%@",[[students objectAtIndex:i] valueForKey:@"evaluateMainId"]] intValue];
        mStudentEvaluateDetail.studentId = [[NSString stringWithFormat:@"%@",[[students objectAtIndex:i] valueForKey:@"studentId"]] intValue];
        mStudentEvaluateDetail.totalScore = [[NSString stringWithFormat:@"%@",[[students objectAtIndex:i] valueForKey:@"totalScore"]] intValue];
        mStudentEvaluateDetail.studentName = [NSString stringWithFormat:@"%@",[[students objectAtIndex:i] valueForKey:@"studentName"]];
        mStudentEvaluateDetail.comment = [NSString stringWithFormat:@"%@",[[students objectAtIndex:i] valueForKey:@"comment"]];
        score += mStudentEvaluateDetail.totalScore;
        [mStudents addObject:mStudentEvaluateDetail];
        [mStudentEvaluateDetail release];
    }
    [students release];
    [((UITableView *)[self.view viewWithTag:100111]) reloadData];
    ((UILabel*)[self.view viewWithTag:10001]).text = [NSString stringWithFormat:@"%d分",score];
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *cellName = @"cellstr";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if (cell== nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
        cell.textLabel.text = @"";
        int textSize = 14;
        [InstanceView addUIImageViewOfPoint:cell.contentView imageFile:@"查看历史学生评价_行.png" point:CGPointMake(0,0)];
        {
            UILabel *label = [InstanceView addLabelOfViewRect:cell.contentView rect:CGRectMake(0, 0, 100, TableviewHeight) string:@"" textSize:textSize textAlignment:UITextAlignmentCenter];
            label.tag = 20011;
        }
        {
            UILabel *label = [InstanceView addLabelOfViewRect:cell.contentView rect:CGRectMake(100, 0, 450, TableviewHeight) string:@"" textSize:textSize textAlignment:UITextAlignmentCenter];
            label.tag = 20022;
        }
        {
            UILabel *label = [InstanceView addLabelOfViewRect:cell.contentView rect:CGRectMake(550, 0, 90, TableviewHeight) string:@"" textSize:textSize textAlignment:UITextAlignmentCenter];
            label.tag = 20033;
        }
        
    }
    if ([indexPath row]<[mStudents count]) {
        StudentEvaluateMain *mStudentEvaluateDetail = [mStudents objectAtIndex:[indexPath row]];
        ((UILabel*)[cell.contentView viewWithTag:20011]).text = mStudentEvaluateDetail.studentName;
        ((UILabel*)[cell.contentView viewWithTag:20022]).text = mStudentEvaluateDetail.comment;
        ((UILabel*)[cell.contentView viewWithTag:20033]).text = [NSString stringWithFormat:@"%d",mStudentEvaluateDetail.totalScore];
    }
    else {
        ((UILabel*)[cell.contentView viewWithTag:20011]).text = @"";
        ((UILabel*)[cell.contentView viewWithTag:20022]).text = @"";
        ((UILabel*)[cell.contentView viewWithTag:20033]).text = @"";
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if ([mStudents count]<11) {
        return 11;
    }
    return [mStudents count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TableviewHeight;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath row]< [mStudents count]) {
        StudentEvaluateMain *mStudentEvaluateDetail = [mStudents objectAtIndex:[indexPath row]];
        NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
        [mDict setObject:mStudentEvaluateDetail.studentName forKey:@"studentName"];
        [mDict setObject:[mDataDict objectForKey:@"name"] forKey:@"name"];
        
        NSLog(@"evaluateMainId -------%d",mStudentEvaluateDetail.evaluateMainId);
        StudentEvaluateDetailController *mEvaluateDetail = [[StudentEvaluateDetailController alloc]initWithNibName:@"StudentEvaluateDetailController" bundle:nil];
        mEvaluateDetail.evaluateMainId = mStudentEvaluateDetail.evaluateMainId;
        mEvaluateDetail.mInfoDict = mDict;
        [mDict release];
        [self.navigationController pushViewController:mEvaluateDetail animated:YES];
        [mEvaluateDetail release];
    }
}
-(void)dealloc
{
    [mDataDict release];
    [mStudents release];
    [mHttpRequest release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mDataDict = nil;
    self.mStudents = nil;
    self.mHttpRequest  = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
