//
//  TeacherSaveEvaluateController.h
//  DMPK
//
//  Created by jiaodian on 12-11-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "InstanceView.h"
#import "InstanceObject.h"
#import "UIPlaceHolderTextView.h"
#import "StarView.h"
@interface TeacherSaveEvaluateController : UIViewController<HttpFormatRequestDeleagte,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
{
    int mItemId;
    
}
@property(nonatomic,assign)int evaluateMainId;

@property(nonatomic,retain)NSMutableArray *mEvaluateDetails,*mStarViewItems;
@property(nonatomic,retain)NSMutableDictionary *mDataDict,*mInfoDict;

@property(nonatomic,retain)HttpFormatRequest *mHttpRequest;

@end

