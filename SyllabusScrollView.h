//
//  SyllabusScrollView.h
//  DMPK
//
//  Created by jiaodian on 12-10-30.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SyllabusScrollViewDelegate <NSObject>

-(void)ResponseScroller:(int)tag;

@end

@interface SyllabusScrollView : UIScrollView
{
    id<SyllabusScrollViewDelegate>delegeta;
}
-(void)initData:(NSMutableArray*)mDataArray;
@property(nonatomic,assign)int maxsectionId;
@property(nonatomic,assign)id<SyllabusScrollViewDelegate>delegeta;
@end

