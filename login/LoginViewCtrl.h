//
//  LoginViewCtrl.h
//  DMPK
//
//  Created by jiaodian on 12-10-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewCtrl : UIViewController<UITextFieldDelegate,HttpFormatRequestDeleagte,UIAlertViewDelegate>

@property(nonatomic,retain)NSMutableDictionary *mLoginDict;
@property(nonatomic,retain)HttpFormatRequest *mHttpRequest;
@end
