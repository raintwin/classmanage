//
//  LoginViewCtrl.m
//  DMPK
//
//  Created by jiaodian on 12-10-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "LoginViewCtrl.h"
#import "SyllabusViewController.h"
#import "RegisterViewController.h"
#import "InstanceView.h"
@interface LoginViewCtrl ()

@end

@implementation LoginViewCtrl
@synthesize mLoginDict;
@synthesize mHttpRequest;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)initLoadView
{
    NSMutableDictionary*mDict = [[NSMutableDictionary alloc] init];
    self.mLoginDict = mDict;
    [mDict release];
    
    HttpFormatRequest *http = [[HttpFormatRequest alloc]init];
    self.mHttpRequest = http;
    [http release];
    [self.view addSubview:mHttpRequest];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"login.png"]]];
    [self initLoadView];
    {
        {
            UITextField *textfield = [InstanceView addTextFileld:CGRectMake(465, 355, 205, 27)];
            textfield.placeholder = @"用户名";
            textfield.delegate = self;
            [textfield setBorderStyle:UITextBorderStyleNone];
            textfield.tag = 101;
            textfield.text = @"TG100001";
            [self.view addSubview:textfield];
            [textfield release];
        }
        {
            UITextField *textfield = [InstanceView addTextFileld:CGRectMake(465, 400, 205, 27)];
            textfield.placeholder = @"密码";
            textfield.delegate = self;
            textfield.secureTextEntry = YES;
            [textfield setBorderStyle:UITextBorderStyleNone];
            textfield.tag = 102;
            textfield.text = @"123456";
            [self.view addSubview:textfield];
            [textfield release];
        }
        
    }
//    {
//        UIButton *mLoginButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"记住密码.png" point:CGPointMake(460, 450)];
//        [mLoginButton addTarget:self action:@selector(MarkPassword:) forControlEvents:64];
//    }
    
//    {
//        UIButton *mLoginButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"忘记密码.png" point:CGPointMake(550, 450)];
//        [mLoginButton addTarget:self action:@selector(ForgetPassword) forControlEvents:64];
//    }
    
    {
        UIButton *mLoginButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"登录按钮.png" point:CGPointMake(462, 485)];
        [mLoginButton addTarget:self action:@selector(LoginView) forControlEvents:64];
    }
//    {
//        UIButton *mLoginButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"注册btn.png" point:CGPointMake(532, 485)];
//        [mLoginButton addTarget:self action:@selector(registerView) forControlEvents:64];
//    }

    [mTeatherInfoDict setObject:[NSNumber numberWithInt:1] forKey:WEBINFOSTRING];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];  
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    //ffff
}
#pragma mark 记住密码
-(void)MarkPassword:sender
{
    
}
#pragma mark 忘记密码
-(void)ForgetPassword
{
    
    


}
-(BOOL)IsJudgeDataIsNull
{
    UITextField *textfield1 = (UITextField*)[self.view viewWithTag:101];
    UITextField *textfield2 = (UITextField*)[self.view viewWithTag:102];
    
    if ([textfield1.text length] == 0) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"用户名不能为空" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alertView.tag = 101;
        alertView.delegate = self;
        [alertView show];
        [alertView release];
        return NO;
    }
    else {
        [mLoginDict setObject:textfield1.text forKey:@"userno"];
    }
    
    if ([textfield2.text length] == 0) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"密码不能为空" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alertView.tag = 102;
        alertView.delegate = self;
        [alertView show];
        [alertView release];
        return NO;
    }
    else {
        [mLoginDict setObject:textfield2.text forKey:@"password"];
    }

    return YES;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    UITextField *textfield1 = (UITextField*)[self.view viewWithTag:101];
    UITextField *textfield2 = (UITextField*)[self.view viewWithTag:102];
    
    if (alertView.tag == 101) {
        [textfield1 becomeFirstResponder];
        return;
    }
    if (alertView.tag == 102) {
        [textfield2 becomeFirstResponder];
        return;
    }

}
#pragma mark 登录
-(void)LoginView
{

    if (![self IsJudgeDataIsNull]) {
        return;
    }
    
    NSString *JSONString = [mLoginDict JSONRepresentation];
    
    mHttpRequest.delegate = self;
    [mHttpRequest CustomFormDataRequestDict:JSONString tag:0 url:[InstanceView FormatUrl:@"ClientLogin"]];
    

}
#pragma mark 注册
-(void)registerView
{
    RegisterViewController *Syllabus = [[RegisterViewController alloc]initWithNibName:@"RegisterViewController" bundle:nil];
    [self.navigationController pushViewController:Syllabus animated:YES];
    [Syllabus release];

}
-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    NSLog(@"HttpRequestFinish-:%@",ResultContent);
    
    
    NSLog(@"ResultContent- id:%d",[[[ResultContent JSONValue] objectForKey:@"id"] intValue]);
    [mTeatherInfoDict setObject:[[ResultContent JSONValue] objectForKey:@"teacherId"] forKey:@"teacherId"];
    
    SyllabusViewController *Syllabus = [[SyllabusViewController alloc]initWithNibName:@"SyllabusViewController" bundle:nil];
    Syllabus.mTeacherId = [[[ResultContent JSONValue] objectForKey:@"teacherId"] intValue];
    [self.navigationController pushViewController:Syllabus animated:YES];
    [Syllabus release];
    

}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
    NSLog(@"HttpRequestFailed-:%@",ResultContent);

}
-(void)dealloc
{
    [mLoginDict release];
    [mHttpRequest release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mLoginDict = nil;
    self.mHttpRequest = nil;
}
-(void)KeyBoardShow
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3f];
    CGRect frame = self.view.frame;
    frame.origin.y = -140;
    [self.view setFrame:frame];
    [UIView commitAnimations];
}
-(void)KeyBoardHide
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3f];
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    [self.view setFrame:frame];
    [UIView commitAnimations];

}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
    NSLog(@"touch text");
    [self KeyBoardShow];
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField;              // called when 'return' key pressed. return NO to ignore.
{
    if (textField.tag == 101) {
        [mLoginDict setObject:textField.text forKey:@"userno"];
    }
    if (textField.tag == 102) {
        [mLoginDict setObject:textField.text forKey:@"password"];
    }
    [self KeyBoardHide];
    [textField resignFirstResponder];
    return YES;
}

- (void)keyboardWillShow:(NSNotification *)notification {  
    [self KeyBoardShow];
}
- (void)keyboardWillHide:(NSNotification *)notification {  
    [self KeyBoardHide];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ( interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown ) {
        return YES;
    }
    else {
        return NO;
    }
}

@end
