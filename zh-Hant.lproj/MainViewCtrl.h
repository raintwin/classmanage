//
//  MainViewCtrl.h
//  DMPK
//
//  Created by jiaodian on 12-10-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstanceObject.h"
#import "SearchClassroomViewController.h"
#import "StudentEvaluateMainController.h"
#import "TeacherSaveEvaluateController.h"
#import "FetchEbookViewController.h"
@interface MainViewCtrl : UIViewController<UIAlertViewDelegate,HttpFormatRequestDeleagte,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,SearchClassroomViewControllerDelegate,UIGestureRecognizerDelegate>
{
    int mClassroomID;
    int mButtonStatusID;
    
    BOOL isMaxWebView;
}

@property(nonatomic,retain)NSMutableArray *mStudents,*mPlans,*mButtons,*mAttendances,*mFetchEbooks,*mButtonTitles;
@property(nonatomic,retain)InstanceObject *mInstanceObject;
@property(nonatomic,retain)NSMutableDictionary *mDataDict,*mInfoDict;

@property(nonatomic,assign)BOOL IsHistory;
@end
