//
//  RegisterViewController.m
//  DMPK
//
//  Created by jiaodian on 12-11-14.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "RegisterViewController.h"
#import "InstanceView.h"
#import "SyllabusViewController.h"
@interface RegisterViewController ()

@end

@implementation RegisterViewController
@synthesize mRegisterDict;
@synthesize mHttpRequest;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)SetBackNav
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"注册页.png"]]];
    {
        NSMutableDictionary*mDict = [[NSMutableDictionary alloc] init];
        self.mRegisterDict = mDict;
        [mDict release];
        
    }
    {
        UIButton *mButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"返回.png" point:CGPointMake(29, 10)];
        [mButton addTarget:self action:@selector(SetBackNav) forControlEvents:64];
    }
    {
        UITextField *textfield = [InstanceView addTextFileld:CGRectMake(485, 355, 205, 27)];
        textfield.delegate = self;
        [textfield setBorderStyle:UITextBorderStyleNone];
        textfield.tag = 101;
        textfield.placeholder = @"用户名";
        [self.view addSubview:textfield];
        [textfield release];
    }
    {
        UITextField *textfield = [InstanceView addTextFileld:CGRectMake(485, 400, 205, 27)];
        textfield.delegate = self;
        [textfield setBorderStyle:UITextBorderStyleNone];
        textfield.tag = 102;
        [textfield setSecureTextEntry:YES];
        textfield.placeholder = @"密码";
        [self.view addSubview:textfield];
        [textfield release];
    }
    {
        UITextField *textfield = [InstanceView addTextFileld:CGRectMake(485, 445, 205, 27)];
        textfield.delegate = self;
        [textfield setSecureTextEntry:YES];
        [textfield setBorderStyle:UITextBorderStyleNone];
        textfield.tag = 103;
        textfield.placeholder = @"确认密码";
        [self.view addSubview:textfield];
        [textfield release];
    }
    {
        UIButton *mLoginButton = [InstanceView addUIButtonByImageAndAutoSize:self.view image:@"注册btn.png" point:CGPointMake(462, 485)];
        [mLoginButton addTarget:self action:@selector(Register) forControlEvents:64];
    }

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];  
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];  

}

-(BOOL)IsJudgeDataIsNull
{
    UITextField *textfield1 = (UITextField*)[self.view viewWithTag:101];
    UITextField *textfield2 = (UITextField*)[self.view viewWithTag:102];
    UITextField *textfield3 = (UITextField*)[self.view viewWithTag:103];

    if ([textfield1.text length] == 0) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"用户名不能为空" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alertView.tag = 101;
        alertView.delegate = self;
        [alertView show];
        [alertView release];
        return NO;
    }
    if ([textfield2.text length] == 0) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"密码不能为空" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alertView.tag = 102;
        alertView.delegate = self;
        [alertView show];
        [alertView release];
        return NO;
    }
    if ([textfield3.text length] == 0) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"确认密码不能为空" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alertView.tag = 103;
        alertView.delegate = self;
        [alertView show];
        [alertView release];
        return NO;
    }
    if (![textfield3.text isEqualToString:textfield2.text]) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"请输入正确的密码" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alertView.tag = 1021;
        alertView.delegate = self;
        [alertView show];
        [alertView release];
        return NO;

    }
    else {
        [mRegisterDict setObject:textfield1.text forKey:@"userno"];
        
    }
    if ([textfield2.text length] == 0) {
        return NO;
    }
    else {
        [mRegisterDict setObject:textfield2.text forKey:@"password"];
        
    }
    return YES;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    UITextField *textfield1 = (UITextField*)[self.view viewWithTag:101];
    UITextField *textfield2 = (UITextField*)[self.view viewWithTag:102];
    UITextField *textfield3 = (UITextField*)[self.view viewWithTag:103];

    if (alertView.tag == 101) {
        [textfield1 becomeFirstResponder];
        return;
    }
    if (alertView.tag == 102) {
        [textfield2 becomeFirstResponder];
        return;
    }
    if (alertView.tag == 103) {
        [textfield3 becomeFirstResponder];
        return;
    }
}
-(void)Register
{
    if (![self IsJudgeDataIsNull]) {
        return;
    }
    
    NSString *JSONString = [mRegisterDict JSONRepresentation];
    
    {
        HttpFormatRequest *http = [[HttpFormatRequest alloc]init];
        http.Delegate = self;
        self.mHttpRequest = http;
        [http release];
        [self.view addSubview:mHttpRequest];
        [mHttpRequest CustomFormDataRequestDict:JSONString tag:0 url:[InstanceView FormatUrl:@"ClientRegister"]];

    }
    


}

-(void)HttpRequestFinish:(NSString *)ResultContent tag:(int)tag
{
    NSLog(@"HttpRequestFinish-:%@",ResultContent);
    
    
    NSLog(@"ResultContent- id:%d",[[[ResultContent JSONValue] objectForKey:@"id"] intValue]);
    [mTeatherInfoDict setObject:[[ResultContent JSONValue] objectForKey:@"teacherId"] forKey:@"teacherId"];
    
    SyllabusViewController *Syllabus = [[SyllabusViewController alloc]initWithNibName:@"SyllabusViewController" bundle:nil];
    Syllabus.mTeacherId = [[[ResultContent JSONValue] objectForKey:@"teacherId"] intValue];
    [self.navigationController pushViewController:Syllabus animated:YES];
    [Syllabus release];
}
-(void)HttpRequestFailed:(NSString *)ResultContent tag:(int)tag
{
    NSLog(@"HttpRequestFailed-:%@",ResultContent);
    
}

-(void)KeyBoardShow
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3f];
    CGRect frame = self.view.frame;
    frame.origin.y = -140;
    [self.view setFrame:frame];
    [UIView commitAnimations];
}
-(void)KeyBoardHide
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3f];
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    [self.view setFrame:frame];
    [UIView commitAnimations];
    
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
{
    NSLog(@"touch text");
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField;              // called when 'return' key pressed. return NO to ignore.
{
    if (textField.tag == 101) {
        [mRegisterDict setObject:textField.text forKey:@"userno"];
    }
    if (textField.tag == 102) {
        [mRegisterDict setObject:textField.text forKey:@"password"];
    }
    [textField resignFirstResponder];
    return YES;
}


- (void)keyboardWillShow:(NSNotification *)notification {  
    [self KeyBoardShow];
}
- (void)keyboardWillHide:(NSNotification *)notification {  
    [self KeyBoardHide];
}
-(void)dealloc
{
    [mRegisterDict release];
    [mHttpRequest release];
    [super dealloc];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.mRegisterDict = nil;
    self.mHttpRequest = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
