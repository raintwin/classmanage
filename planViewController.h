//
//  planViewController.h
//  DMPK
//
//  Created by applebaba on 13-11-11.
//
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"
@interface DirectoryData : NSObject

@property(nonatomic,retain)NSString *createBy,*name;
@property(nonatomic,assign)int did,date,day,hours,minutes,month,nanos,seconds,time,timezoneOffset,year,pid,seniorId,subjectId,type,versionId;

@end

@interface planViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,HttpFormatRequestDeleagte>



@property(nonatomic,assign) int versionId ,subjectId ,seniorId,pid,catalogId;
@property(nonatomic,assign) BOOL isBook;
@property(nonatomic,retain) IBOutlet UITableView *tableView;
@property(nonatomic,retain) IBOutlet UIButton *backButton;
@property(nonatomic,retain) IBOutlet UILabel *titleLabel,*tipLabel;

@property(nonatomic,retain) NSString *cleintRequest,*titleString;

@property(nonatomic,retain) NSMutableArray *directoryDatas;

- (IBAction)backButton:(id)sender;
@end
