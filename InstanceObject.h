//
//  InstanceObject.h
//  DMPK
//
//  Created by jiaodian on 12-10-30.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstanceObject : NSObject
@property(nonatomic,retain)NSString *adjDate,*gradeName,*isDefault,*isSingleWeek,*subjectName;
@property(nonatomic,assign)int gradeId,sectionId,subjectId,weekId,seniorId;
@end

@interface InstanceStudent : NSObject
@property(nonatomic,assign)int colsId,rowsId,studentId,status,getderCode;
@property(nonatomic,retain)NSString *studentName,*gender;
@property(nonatomic,assign)BOOL IsMark;

@end

@interface PlanObject : NSObject

@property(nonatomic,retain)NSString *name,*subjectName,*teacherName;
@property(nonatomic,assign)int planId;

@end



@interface ButtonObject : NSObject

@property(nonatomic,retain)NSString *name,*url;
@property(nonatomic,assign)int mID;

@end



@interface StudentEvaluateMain : NSObject
@property(nonatomic,assign)int evaluateMainId,studentId,totalScore;
@property(nonatomic,retain)NSString *studentName,*comment;
@property(nonatomic,assign)BOOL IsMark;

@end 


@interface StudentEvaluateDetail : NSObject
@property(nonatomic,assign)int itemId,evaluateId,score,evaluateScore;
@property(nonatomic,retain)NSString *itemDescription,*description;

@property(nonatomic,assign)BOOL IsMark;

@end    



@interface FetchEbook : NSObject
@property(nonatomic,retain)NSString *name,*minUrl,*url;
@property(nonatomic,retain)UIImage *image;
@property(nonatomic,assign)BOOL isLoad;
@end 
